import pytest
import pandas as pd


def pytest_addoption(parser):
    parser.addoption(
        "--runslow", action="store_true", default=False, help="run slow tests"
    )
    parser.addoption(
        "--veryslow", action="store_true", default=False, help="run very slow tests"
    )


def pytest_configure(config):
    config.addinivalue_line("markers", "slow: mark test as slow to run")
    config.addinivalue_line("markers", "vslow: mark test as very slow to run")


def pytest_collection_modifyitems(config, items):
    if config.getoption("--veryslow"):
        # --runslow given in cli: do not skip slow or very slow tests
        return
    elif config.getoption("--runslow"):
        skip_very_slow = pytest.mark.skip(reason="need --veryslow option to run")
        for item in items:
            if "vslow" in item.keywords:
                item.add_marker(skip_very_slow)
        return
    skip_slow = pytest.mark.skip(reason="need --runslow option to run")
    for item in items:
        if "slow" in item.keywords or "vslow" in item.keywords:
            item.add_marker(skip_slow)


@pytest.fixture
def genome_names():
    return "Sp7498_HiC_Chr19", "Sp9509_oxford_v3_Chr19", "Sp9512_a02_genome_Chr19"


@pytest.fixture
def adj_matrix(genome_names):
    return pd.DataFrame(
        [
            [3793342, 3050871, 3051955],
            [3050871, 3811527, 3339780],
            [3051955, 3339780, 3739816],
        ],
        index=genome_names,
        columns=genome_names,
    )


@pytest.fixture
def adj_matrix_subset(genome_names):
    return pd.DataFrame(
        [[3811527, 3339780], [3339780, 3739816]],
        index=genome_names[1:],
        columns=genome_names[1:],
    )


@pytest.fixture
def adj_matrix_exclusive_subset(genome_names):
    return pd.DataFrame(
        [[760656, 476995], [476995, 687861]],
        index=genome_names[1:],
        columns=genome_names[1:],
    )



@pytest.fixture
def adj_matrix_k15(genome_names):
    return pd.DataFrame(
        [
            [3543939, 3185075, 3179294],
            [3185075, 3576033, 3331308],
            [3179294, 3331308, 3530186],
        ],
        index=genome_names,
        columns=genome_names,
    )


@pytest.fixture()
def upper():
    return 4158743107228753097


@pytest.fixture()
def lower():
    return 1
