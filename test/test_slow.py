import pytest
import pandas as pd
import tarfile
from itertools import product, islice
from multiprocessing import cpu_count
from pankmer.download import download_example
from pankmer.pankmer import PKResults, index_wrapper
from pankmer.count import count_kmers
from pankmer.collect import collect
from pankmer.upset import count_scores
from pankmer.adjacency_matrix import get_adjacency_matrix
from pankmer.anchor import anchor_region, anchor_genome
from pankmer.subset import subset
from pankmer.index import load_metadata


@pytest.fixture(scope="session")
def example_data(tmp_path_factory):
    d = tmp_path_factory.mktemp("pk_example")
    download_example(d)
    return d / "PanKmer_example_Sp_Chr19"


@pytest.fixture(scope="session")
def example_index(example_data):
    index_wrapper(
        example_data / "Sp_Chr19_index",
        genomes_input=example_data / "Sp_Chr19_genomes",
        threads=cpu_count(),
    )
    return example_data / "Sp_Chr19_index"

@pytest.fixture(scope="session")
def example_index_k15(example_data):
    index_wrapper(
        example_data / "Sp_Chr19_index_k15",
        genomes_input=example_data / "Sp_Chr19_genomes",
        k=15,
        threads=cpu_count(),
    )
    return example_data / "Sp_Chr19_index_k15"


@pytest.fixture(scope="session")
def example_index_split(example_data):
    index_wrapper(
        example_data / "Sp_Chr19_index_split",
        genomes_input=example_data / "Sp_Chr19_genomes",
        threads=cpu_count(),
        split_memory=8,
    )
    return example_data / "Sp_Chr19_index_split"


@pytest.fixture(scope="session")
def example_index_tar(example_data, example_index):
    with tarfile.open(example_data / "Sp_Chr19_index.tar", "w") as tar:
        tar.add(example_index)
    return example_data / "Sp_Chr19_index.tar"


@pytest.fixture(scope="session")
def example_subset(example_data, example_index):
    results = PKResults(example_index)
    subset(
        str(example_index),
        str(example_data / "Sp_Chr19_subset"),
        tuple(islice(results.genomes, 1, None)),
    )
    return example_data / "Sp_Chr19_subset"


@pytest.fixture(scope="session")
def example_exclusive_subset(example_data, example_index):
    results = PKResults(example_index)
    subset(
        str(example_index),
        str(example_data / "Sp_Chr19_exclusive_subset"),
        tuple(islice(results.genomes, 1, None)),
        exclusive=True,
    )
    return example_data / "Sp_Chr19_exclusive_subset"


@pytest.fixture(scope="session")
def example_subset_from_tar(example_data, example_index_tar):
    results = PKResults(example_index_tar)
    subset(
        str(example_index_tar),
        str(example_data / "Sp_Chr19_subset_from_tar"),
        tuple(islice(results.genomes, 1, None)),
    )
    return example_data / "Sp_Chr19_subset_from_tar"


@pytest.fixture
def kmer_counts():
    return pd.DataFrame(
        ((1047828,), (854251,), (2862785,)),
        columns=("Sp_Chr19_index",),
        index=range(1, 4),
    )


@pytest.fixture
def kmer_counts_k15():
    return pd.DataFrame(
        ((462960,), (491521,), (3068052,)),
        columns=("Sp_Chr19_index",),
        index=range(1, 4),
    )


@pytest.fixture
def kmer_counts_subset():
    return pd.DataFrame(
        ((871783,), (3339780,)), columns=("Sp_Chr19_subset",), index=range(1, 3)
    )


@pytest.fixture
def kmer_counts_exclusive_subset():
    return pd.DataFrame(
        ((494527,), (476995,)),
        columns=("Sp_Chr19_exclusive_subset",),
        index=range(1, 3),
    )


@pytest.fixture
def collect_stats():
    return pd.DataFrame(
        [
            [1, 3781561.666666667, "Pan"],
            [1, 3781561.6666666665, "Core"],
            [2, 4415588.0, "Pan"],
            [2, 3147535.3333333335, "Core"],
            [3, 4764864.0, "Pan"],
            [3, 2862785.0, "Core"],
        ],
        columns=("Genomes", "k-mers", "sequence"),
    )


@pytest.fixture
def collect_stats_k15():
    return pd.DataFrame(
        [
            [1, 3550052.666666667, "Pan"],
            [1, 3550052.6666666665, "Core"],
            [2, 3868213.0, "Pan"],
            [2, 3231892.3333333335, "Core"],
            [3, 4022533.0, "Pan"],
            [3, 3068052.0, "Core"],
        ],
        columns=("Genomes", "k-mers", "sequence"),
    )



@pytest.fixture
def collect_stats_subset():
    return pd.DataFrame(
        [
            [1, 3775671.5, "Pan"],
            [1, 3775671.5, "Core"],
            [2, 4211563.0, "Pan"],
            [2, 3339780.0, "Core"],
        ],
        columns=("Genomes", "k-mers", "sequence"),
    )


@pytest.fixture
def collect_stats_exclusive_subset():
    return pd.DataFrame(
        [
            [1, 724258.5, "Pan"],
            [1, 724258.5, "Core"],
            [2, 971522.0, "Pan"],
            [2, 476995.0, "Core"],
        ],
        columns=("Genomes", "k-mers", "sequence"),
    )


@pytest.fixture
def collect_stats_conf():
    return pd.DataFrame(
        [
            [1, 3739816.0, "Pan"],
            [2, 4283764.5, "Pan"],
            [3, 4494630.5, "Pan"],
            [1, 3739816.0, "Core"],
            [2, 3195867.5, "Core"],
            [3, 2862785.0, "Core"],
            [1, 3811527.0, "Pan"],
            [2, 4427728.5, "Pan"],
            [3, 4711389.5, "Pan"],
            [1, 3811527.0, "Core"],
            [2, 3195325.5, "Core"],
            [3, 2862785.0, "Core"],
            [1, 3793342.0, "Pan"],
            [2, 4535271.0, "Pan"],
            [3, 5088572.0, "Pan"],
            [1, 3793342.0, "Core"],
            [2, 3051413.0, "Core"],
            [3, 2862785.0, "Core"],
        ],
        columns=("Genomes", "k-mers", "sequence"),
    )


@pytest.fixture
def upset_stats():
    return pd.Series(
        [476995, 2862785, 283661, 188086, 189170, 553301, 210866],
        index=pd.MultiIndex.from_tuples(
            [
                (False, True, True),
                (True, True, True),
                (False, True, False),
                (True, True, False),
                (True, False, True),
                (True, False, False),
                (False, False, True),
            ]
        ),
    )


@pytest.fixture
def upset_stats_subset():
    return pd.Series(
        [3339780, 471747, 400036],
        index=pd.MultiIndex.from_tuples([(True, True), (True, False), (False, True)]),
    )


@pytest.fixture
def upset_stats_exclusive_subset():
    return pd.Series(
        [476995, 283661, 210866],
        index=pd.MultiIndex.from_tuples([(True, True), (True, False), (False, True)]),
    )


@pytest.fixture
def anchor_region_top_rows():
    return pd.DataFrame(
        [
            ["Chr19", 384628, 384629, 100.0],
            ["Chr19", 384629, 384630, 100.0],
            ["Chr19", 384630, 384631, 100.0],
            ["Chr19", 384631, 384632, 100.0],
        ]
    )


@pytest.fixture
def anchor_genome_rounded_k15():
    return pd.DataFrame(
        [
            ["Chr19", 0.000000, 96.291874, 0, 0],
            ["Chr19", 0.252561, 91.472425, 0, 0],
            ["Chr19", 0.505123, 95.186838, 0, 0],
            ["Chr19", 0.757684, 94.436528, 0, 0],
            ["Chr19", 1.000000, 96.007985, 0, 0],
        ],
        columns=("SeqID", "Chromosome", "K-mer conserv (%)", "Group", "Group_chrom"),
    )


@pytest.fixture
def anchor_genome_rounded():
    return pd.DataFrame(
        [
            ["Chr19", 0.000000, 92.272615, 0, 0],
            ["Chr19", 0.252561, 84.937552, 0, 0],
            ["Chr19", 0.505123, 90.537676, 0, 0],
            ["Chr19", 0.757684, 89.768623, 0, 0],
            ["Chr19", 1.000000, 91.644462, 0, 0],
        ],
        columns=("SeqID", "Chromosome", "K-mer conserv (%)", "Group", "Group_chrom"),
    )


@pytest.fixture
def kmers_to_get():
    return {2412807660169719871: 1, 3476510368925482027: 1, 2234894531119824834: 1}


@pytest.fixture
def metadata(example_data):
    return {
        "kmer_size": 31,
        "genomes": {
            0: "Sp7498_HiC_Chr19",
            1: "Sp9509_oxford_v3_Chr19",
            2: "Sp9512_a02_genome_Chr19",
        },
        "genome_sizes": {
            "Sp7498_HiC_Chr19": 3993341,
            "Sp9509_oxford_v3_Chr19": 3959434,
            "Sp9512_a02_genome_Chr19": 3855210,
        },
        "positions": {4611686018427387901: 4764862, 4611686018427387903: 4764863},
        "mem_blocks": [
            [0, 4611686018427387903],
            [4611686018427387903, 9223372036854775807],
        ],
        "fraction_cutoff": [1, 2**64 - 1],
    }


@pytest.mark.slow
def test_index_from_tar_file(example_data):
    with tarfile.open(example_data / "Sp_Chr19_genomes.tar", "w") as tar:
        tar.add(example_data / "Sp_Chr19_genomes", arcname="Sp_Chr19_genomes")
    index_wrapper(
        example_data / "Sp_Chr19_index.tar",
        genomes_input=example_data / "Sp_Chr19_genomes.tar",
        threads=cpu_count(),
    )


@pytest.mark.slow
def test_count(example_index, example_index_split, example_index_k15, kmer_counts, kmer_counts_k15):
    results = PKResults(example_index)
    results_split = PKResults(example_index_split)
    results_k15 = PKResults(example_index_k15)
    test_kmer_counts = count_kmers(results, names=("Sp_Chr19_index",))
    test_kmer_counts_split = count_kmers(results_split, names=("Sp_Chr19_index",))
    test_kmer_counts_k15 = count_kmers(results_k15, names=("Sp_Chr19_index",))
    for column in ("Sp_Chr19_index",):
        for row in range(1, 4):
            assert test_kmer_counts.loc[row, column] == kmer_counts.loc[row, column]
            assert (
                test_kmer_counts_split.loc[row, column] == kmer_counts.loc[row, column]
            )
            assert test_kmer_counts_k15.loc[row, column] == kmer_counts_k15.loc[row, column]


@pytest.mark.slow
def test_count_subset(
    example_subset,
    example_subset_from_tar,
    example_exclusive_subset,
    kmer_counts_subset,
    kmer_counts_exclusive_subset,
):
    results = PKResults(example_subset)
    results_from_tar = PKResults(example_subset_from_tar)
    results_exclusive = PKResults(example_exclusive_subset)
    test_kmer_counts = count_kmers(results, names=("Sp_Chr19_subset",))
    test_kmer_counts_from_tar = count_kmers(
        results_from_tar, names=("Sp_Chr19_subset",)
    )
    test_kmer_counts_ex = count_kmers(
        results_exclusive, names=("Sp_Chr19_exclusive_subset",)
    )
    for column in ("Sp_Chr19_subset",):
        for row in range(1, 3):
            assert (
                test_kmer_counts.loc[row, column] == kmer_counts_subset.loc[row, column]
            )
            assert (
                test_kmer_counts_from_tar.loc[row, column]
                == kmer_counts_subset.loc[row, column]
            )
    for column in ("Sp_Chr19_exclusive_subset",):
        for row in range(1, 3):
            assert (
                test_kmer_counts_ex.loc[row, column]
                == kmer_counts_exclusive_subset.loc[row, column]
            )


@pytest.mark.slow
def test_count_from_tar_file(example_index_tar, kmer_counts):
    results = PKResults(example_index_tar)
    test_kmer_counts = count_kmers(results, names=("Sp_Chr19_index",))
    for column in ("Sp_Chr19_index",):
        for row in range(1, 4):
            assert test_kmer_counts.loc[row, column] == kmer_counts.loc[row, column]


@pytest.mark.slow
def test_collect(example_data, example_index, example_index_split,
                 example_index_k15, collect_stats, collect_stats_k15):
    results = PKResults(example_index)
    results_split = PKResults(example_index_split)
    results_k15 = PKResults(example_index_k15)
    test_col = collect(results, output=(example_data / "Sp_Chr19_col.svg"))
    test_col_split = collect(results_split, output=(example_data / "Sp_Chr19_col.svg"))
    test_col_k15 = collect(results_k15, output=(example_data / "Sp_Chr19_col.svg"))
    for column in "sequence", "Genomes", "k-mers":
        for i in range(6):
            assert test_col.loc[i, column] == collect_stats.loc[i, column]
            assert test_col_split.loc[i, column] == collect_stats.loc[i, column]
            assert test_col_k15.loc[i, column] == collect_stats_k15.loc[i, column]


@pytest.mark.slow
def test_collect_subset(
    example_data,
    example_subset,
    example_subset_from_tar,
    example_exclusive_subset,
    collect_stats_subset,
    collect_stats_exclusive_subset,
):
    results = PKResults(example_subset)
    results_from_tar = PKResults(example_subset_from_tar)
    results_exclusive = PKResults(example_exclusive_subset)
    test_col_subset = collect(results, output=(example_data / "Sp_Chr19_col.svg"))
    test_col_subset_from_tar = collect(
        results_from_tar, output=(example_data / "Sp_Chr19_col.svg")
    )
    test_col_subset_ex = collect(
        results_exclusive, output=(example_data / "Sp_Chr19_col.svg")
    )
    for column in "sequence", "Genomes", "k-mers":
        for i in range(4):
            assert test_col_subset.loc[i, column] == collect_stats_subset.loc[i, column]
            assert (
                test_col_subset_from_tar.loc[i, column]
                == collect_stats_subset.loc[i, column]
            )
            assert (
                test_col_subset_ex.loc[i, column]
                == collect_stats_exclusive_subset.loc[i, column]
            )


@pytest.mark.slow
def test_upset(
    example_index,
    example_index_split,
    upset_stats,
    upset_stats_subset,
    upset_stats_exclusive_subset,
):
    results = PKResults(example_index)
    results_split = PKResults(example_index_split)
    test_upset = count_scores(results, tuple(results.genomes))
    test_upset_split = count_scores(results_split, tuple(results_split.genomes))
    test_upset_subset = count_scores(results, tuple(islice(results.genomes, 1, None)))
    test_upset_subset_ex = count_scores(
        results, tuple(islice(results.genomes, 1, None)), exclusive=True
    )
    for i in upset_stats.index:
        assert test_upset.loc[i] == upset_stats.loc[i]
        assert test_upset_split.loc[i] == upset_stats.loc[i]
    for i in upset_stats_subset.index:
        assert test_upset_subset.loc[i] == upset_stats_subset.loc[i]
    for i in upset_stats_exclusive_subset.index:
        assert test_upset_subset_ex.loc[i] == upset_stats_exclusive_subset.loc[i]


@pytest.mark.slow
def test_upset_subset(
    example_subset,
    example_subset_from_tar,
    example_exclusive_subset,
    upset_stats_subset,
    upset_stats_exclusive_subset,
):
    results = PKResults(example_subset)
    results_from_tar = PKResults(example_subset_from_tar)
    results_exclusive = PKResults(example_exclusive_subset)
    test_upset_subset = count_scores(results, tuple(results.genomes))
    test_upset_subset_from_tar = count_scores(results_from_tar, tuple(results.genomes))
    test_upset_subset_ex = count_scores(
        results_exclusive, tuple(results_exclusive.genomes)
    )
    for i in upset_stats_subset.index:
        assert test_upset_subset.loc[i] == upset_stats_subset.loc[i]
        assert test_upset_subset_from_tar.loc[i] == upset_stats_subset.loc[i]
    for i in upset_stats_exclusive_subset.index:
        assert test_upset_subset_ex.loc[i] == upset_stats_exclusive_subset.loc[i]


@pytest.mark.slow
def test_collect_conf(example_index, collect_stats_conf):
    results = PKResults(example_index)
    test_col = collect(results, conf=True)
    for column in "sequence", "Genomes", "k-mers":
        for i in range(18):
            assert test_col.loc[i, column] == collect_stats_conf.loc[i, column]


@pytest.mark.slow
def test_collect_from_tar_file(example_index_tar, collect_stats):
    results = PKResults(example_index_tar)
    test_kmer_counts = collect(results)
    for column in "sequence", "Genomes", "k-mers":
        for i in range(6):
            assert test_kmer_counts.loc[i, column] == collect_stats.loc[i, column]


@pytest.mark.slow
def test_adjacency_matrix(example_index, example_index_split, example_index_k15,
                          adj_matrix, adj_matrix_k15):
    test_adj_matrix = get_adjacency_matrix(example_index)
    test_adj_matrix_split = get_adjacency_matrix(example_index_split)
    test_adj_matrix_k15 = get_adjacency_matrix(example_index_k15)
    for i, j in product(range(3), repeat=2):
        assert test_adj_matrix.iloc[i, j] == adj_matrix.iloc[i, j]
        assert test_adj_matrix_split.iloc[i, j] == adj_matrix.iloc[i, j]
        assert test_adj_matrix_k15.iloc[i, j] == adj_matrix_k15.iloc[i, j]


@pytest.mark.slow
def test_adjacency_matrix_subset(
    example_subset,
    example_subset_from_tar,
    example_exclusive_subset,
    adj_matrix_subset,
    adj_matrix_exclusive_subset,
):
    test_adj_matrix_subset = get_adjacency_matrix(example_subset)
    test_adj_matrix_subset_from_tar = get_adjacency_matrix(example_subset_from_tar)
    test_adj_matrix_subset_ex = get_adjacency_matrix(example_exclusive_subset)
    for i, j in product(range(2), repeat=2):
        assert test_adj_matrix_subset.iloc[i, j] == adj_matrix_subset.iloc[i, j]
        assert (
            test_adj_matrix_subset_from_tar.iloc[i, j] == adj_matrix_subset.iloc[i, j]
        )
        assert (
            test_adj_matrix_subset_ex.iloc[i, j]
            == adj_matrix_exclusive_subset.iloc[i, j]
        )


@pytest.mark.slow
def test_anchor_region(example_data, example_index, anchor_region_top_rows):
    conserv = pd.DataFrame(anchor_region(str(example_index),
        anchor=example_data / 'Sp_Chr19_genomes' / 'Sp9509_oxford_v3_Chr19.fasta.gz',
        coords='Chr19:384629-385934'))
    for i, j in product(range(4), repeat=2):
        assert conserv.iloc[i, j] == anchor_region_top_rows.iloc[i, j]


@pytest.mark.slow
def test_anchor_region_gene_name(example_data, example_index, anchor_region_top_rows):
    conserv = pd.DataFrame(anchor_region(str(example_index),
        anchor=example_data / 'Sp_Chr19_genomes' / 'Sp9509_oxford_v3_Chr19.fasta.gz',
        coords='Sp19g00080',
        genes=example_data / 'Sp_Chr19_features' / 'Sp9509_oxford_v3_Chr19.gff3.gz'))
    for i, j in product(range(4), repeat=2):
        assert conserv.iloc[i, j] == anchor_region_top_rows.iloc[i, j]


@pytest.mark.slow
def test_anchor_region_to_file(example_data, example_index, anchor_region_top_rows):
    anchor_region(str(example_index),
        anchor=example_data / 'Sp_Chr19_genomes' / 'Sp9509_oxford_v3_Chr19.fasta.gz',
        coords='Chr19:384629-385934',
        output_file=example_data / 'regcov.bdg')
    regcov = pd.read_table(example_data / 'regcov.bdg', header=None).head(4)
    for i, j in product(range(4), repeat=2):
        assert regcov.iloc[i, j] == anchor_region_top_rows.iloc[i, j]


@pytest.mark.slow
def test_retrieve_metadata(example_data, metadata):
    index_wrapper(
        example_data / "Sp_Chr19_index_singlethread",
        genomes_input=example_data / "Sp_Chr19_genomes",
        threads=1,
        split_memory=1,
    )
    meta = load_metadata(str(example_data / "Sp_Chr19_index_singlethread"), "")
    assert meta.kmer_size == metadata["kmer_size"]
    assert meta.genomes == metadata["genomes"]
    assert meta.genome_sizes == metadata["genome_sizes"]
    assert meta.positions == metadata["positions"]
    assert meta.mem_blocks == metadata["mem_blocks"]


@pytest.mark.vslow
def test_anchor_genome_k15(example_data, example_index_k15, anchor_genome_rounded_k15):
    anchor_genome(str(example_index_k15),
        anchor=str(example_data / 'Sp_Chr19_genomes' / 'Sp9509_oxford_v3_Chr19.fasta.gz'),
        chromosomes=['Chr19'],
        output=example_data / 'anchor_genome.svg',
        output_table=example_data / 'anchor_genome.tsv')
    conserv = pd.read_table(example_data / 'anchor_genome.tsv')
    for i in range(4):
        assert conserv.loc[i, "SeqID"] == anchor_genome_rounded_k15.loc[i, "SeqID"]
    for i, j in product(range(5), range(1, 5)):
        assert round(conserv.iloc[i, j], 6) == anchor_genome_rounded_k15.iloc[i, j]



@pytest.mark.vslow
def test_anchor_genome(example_data, example_index, anchor_genome_rounded):
    anchor_genome(str(example_index),
        anchor=str(example_data / 'Sp_Chr19_genomes' / 'Sp9509_oxford_v3_Chr19.fasta.gz'),
        chromosomes=['Chr19'],
        output=example_data / 'anchor_genome.svg',
        output_table=example_data / 'anchor_genome.tsv')
    conserv = pd.read_table(example_data / 'anchor_genome.tsv')
    for i in range(4):
        assert conserv.loc[i, "SeqID"] == anchor_genome_rounded.loc[i, "SeqID"]
    for i, j in product(range(5), range(1, 5)):
        assert round(conserv.iloc[i, j], 6) == anchor_genome_rounded.iloc[i, j]
