# PanKmer example data

Included in this dataset are sequences from _Spirodela polyrhiza_
clones Sp7498, Sp9509, Sp9512. Sp7498 and
Sp9509 sequences were sourced from the following references found at
[http://spirodelagenome.org](http://spirodelagenome.org):

```
Sp9509_oxford_v3
NCBI: GCA_900492545.1
CoGe: id51364
This genome was generated with Oxford Nanopore and polished with Illumina, scaffolded against the previous Illumina-based genome Sp9509v3 and validated with BioNano optical maps and multi-color FISH (mcFISH).

Hoang PNT, Michael TP, Gilbert S, Chu P, Motley TS, Appenroth KJ, Schubert I, Lam E. Generating a high-confidence reference genome map of the Greater Duckweed by integration of cytogenomic, optical mapping and Oxford Nanopore technologies. Plant J. 2018 Jul 28.

Sp7498_HiC
CoGe: 55877
This assembly was generated using Oxford Nanopore long reads and Illumina-based HiC scaffolding.

Harkess A, McGlaughlin F, Bilkey N, Elliott K, Emenecker R, Mattoon E, Miller K, Vierstra R, Meyers BC, Michael TP. High contiguity Spirodela polyrhiza genomes reveal conserved chromosomal structure. Submitted.
```

Sp9512 sequence was sourced from research data for the following in-progress publication:

```
Pasaribu B, Acosta K, Aylward A, Abramson BW, Colt K, Hartwick NT, Liang Y, Shanklin J, Michael TP, Lam E Genomics of turions from the Greater Duckweed reveal pathways for tissue dormancy and reemergence strategy of an aquatic plant.
```

Sp9512 can be downloaded from [Michael lab AWS storage](https://salk-tm-pub.s3.us-west-2.amazonaws.com/duckweed/Sp9512.a02_final.tar).
