Limitations
===========

*k*-mers and genetic variants
-----------------------------

The presence or absence of 31-mers can be used to detect most cases of SNP, INDEL, and SV including deletion, insertion, inversion, and translocation. See below illustrations of how it works. These figures were published in [#vw]_ and [#klb]_ .

.. figure:: _static/voichek-weigel-kmer-var.png
   :alt: *k*-mers and different genetic variants

   **k-mers and genetic variants.** The blue and red lines represent two individual genomes. The colored short bars mark *k*-mers unique to each genome, and the gray bars mark *k*-mers shared between genomes. From [#vw]_

.. figure:: _static/karikari-kmer-var.png
   :alt: Illustration of the location of unique *k*-mers

   **k-mers and genetic variants.** Illustration of the location of unique *k*-mers (red lines) originating from a reference genome (solid black lines) and an alternate genome (solid blue lines) depending on the underlying variant type. Dashed lines indicate genomic locations (vertical lines) or ranges of genomic positions (horizontal lines) that will induce unique *k*-mer patterns in the respective sample. (a) A single-nucleotide polymorphism (SNP, indicated by ticks on the genomes) will result in *k*-mers specific to each genome when *k*-mers overlap with the SNP. (b) For insertions/deletions, unique *k*-mers will originate from the breakpoints induced by the variation. In addition, if the sequence insertion is novel (i.e., not found elsewhere in the genome), unique *k*-mers will also originate from within the inserted sequence. (c) For inversions, unique *k*-mers arise at the inversion breakpoints. *k*-mers originating from within the inverted sequence will not differentiate between the two genomes because only the orientation of the sequence will differ. (d) For tandem duplications, novel adjacencies will only occur in the genome bearing the additional copies. From [#klb]_


Copy number variation
---------------------

PanKmer and similar methods can be confounded by copy number variations in repetitive sequences. *k*-mer presence/absence cannot tag CNVs unless their junctions produce unique K-mers. In practice, while some special cases can be tagged, a large class of CNVs are invisible to PanKmer. See an illustrative example below:

.. figure:: _static/pankmer_copy_number.*
   :alt: Limitations of *k*-mers for tagging copy number variation

   **Limitations of k-mers for tagging copy number variation.** Genomes G1, G2, and G3 have 1, 2, and 3 tandem copies of a repeat, respectively. K-mers produced by the copy junction highlighted in magenta can distinguish G2 and G3 from G1. However, G2 cannot be distinguished from G3 since the junction is present in both.

.. [#vw] Voichek, Y. & Weigel, D. Identifying genetic variants underlying phenotypic variation in plants without complete genomes. *Nat. Genet.* 52, 534-540 (2020).

.. [#klb] Karikari B, Lemay M-A, Belzile F. k-mer-Based Genome-Wide Association Studies in Plants: Advances, Challenges, and Perspectives. Genes. 2023; 14(7):1439.