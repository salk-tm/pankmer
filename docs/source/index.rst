.. PanKmer documentation master file, created by
   sphinx-quickstart on Mon Jun  6 19:04:32 2022.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

PanKmer documentation
=====================

Overview
--------

The declining cost of DNA sequencing and proliferation of high-quality genome
assemblies has given rise to the field of
`pangenomics <https://www.sciencedirect.com/science/article/abs/pii/S016895251930246X>`_ ,
which concerns the entire collection of genomes across one or more species
rather than the genome of an individual organism.

One useful method for representing a pangenome is as a table of *k*-mers,
which are nucleotide sequences of a given length *k*. The pangenome can be
efficiently represented by a *k*-mer index. The index comprises a list of all
unique k-mers observed across the population together with a table
indicating presence or absence of each *k*-mer in each individual genome.
**PanKmer** is a tool for generating and analyzing *k*-mer indexes.

Source code is available from the `PanKmer GitLab repository <https://gitlab.com/salk-tm/pankmer>`_ .

Published in *Bioinformatics*: `PanKmer: k-mer based and reference-free pangenome analysis <https://academic.oup.com/bioinformatics/article/39/10/btad621/7319363>`_ .

Please submit questions to the `Issues page on GitLab <https://gitlab.com/salk-tm/pankmer/-/issues>`_.

Primary contact: Todd P. Michael, tmicha@salk.edu

.. figure:: _static/pankmer_diagram_explicit_bitvec.png
   :alt: Diagram of PanKmer functionality

   Diagram of PanKmer functionality.

Contents
--------

.. toctree::

   installation
   dependencies
   tutorial
   examples
   anchormap
   iteration
   large_pangenomes
   datasets
   raw_reads
   algorithm
   limitations
   tests
   cli
   api
