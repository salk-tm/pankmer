Large pangenomes
================

.. role:: zsh(code)
   :language: zsh

When building a pangenome of more than a handful of input genomes, you may wish
to adjust some settings to save on resource usage.

*k*-mer counting rounds
-----------------------
A challenge of building the PanKmer index from a large set of eukaryote genomes is the significant memory footprint of the operation. PanKmer includes the :zsh:`-–rounds` option to regulate peak memory usage. The default value of :zsh:`-–rounds` is 1, which means each input genome will be read once by each thread and the entire index will be built on the first pass. Increasing the value will divide the *k*-mer space into discrete blocks and construct a sub-index for each block, in sequence, finally merging all sub-indexes into the complete index. When the number of rounds is greater than 1, this scheme reduces peak memory usage by reducing the number of *k*-mers held in memory, at the expense of increased runtime since multiple *k*-mer decomposition passes must be made over each genome. Hence, when the pangenome size is small or large memory resources are available, fewer rounds are recommended, while more rounds are recommended for large pangenomes or environments with minimal memory.

.. code-block:: zsh

   pankmer index --rounds <nrounds> -g <genomes> -o <index[.tar]>

Estimate the appropriate number of rounds based on the size of your pangenome
and your available memory resources. See the benchmarks below for a starting
point. Increasing the number of rounds will decrease memory usage at the expense
of longer runtime, while decreasing the number of rounds will decrease runtime
at the expense of higher memory usage. Pankmer can use up to hundreds of
rounds to achieve low memory usage for large pangenomes.

GZIP compression
----------------
Depending on your pangenome size and computing resources, you may wish to
adjust the gzip compression level of indexing. You can do this by:

.. code-block:: zsh

   pankmer index --gzip-level <level> -g <genomes> -o <index[.tar]>

The gzip level is an integer from 1-9, with a default value of 6. Setting a
lower gzip level will shorten runtime but will slightly increase the size of
the PanKmer index on disk. Setting a higher gzip level will slightly decrease
the size of the index but will increase runtime. For fastest indexing usage
:zsh:`--gzip-level 1`.

Index completeness
------------------

By default, PanKmer records *every* *k*-mer observed in the input sequences,
producing an index that is "*k*-mer complete".
This can be redundant for some applications. In such cases, you can use the
:zsh:`-q/--qual` argument to reduce completeness of the index.

.. code-block:: zsh

   pankmer index -q <Q> -g <genomes> -o <index[.tar]>
   pankmer index -q 30 -g <genomes> -o <index[.tar]>

In a "Q30" PanKmer index, 80% of k-mers will be discarded instead of recorded.
Approx 999 of every 1000 base pairs of input sequence will still be represented
in at least 1 recorded k-mer. This allows a significant reduction in required
memory and runtime, with only a slight loss in fidelity.

Alternatively, set the fraction of k-mers directly:

.. code-block:: zsh

   pankmer index --fraction <float> -g <genomes> -o <index[.tar]>

Benchmarks
----------

PanKmer was benchmarked on several pangenome datasets of various sizes.

1. The Typha super-pangenome dataset presented in the :ref:`use case <use_case>` (13 genomes total)
2. *Solanum* super-pangenome (46 genomes total)
3. *Zea* super-pangenome (54 genomes total)
4. *H. sapiens* pangenome (94 haplotypes total)
5. *A. thaliana* pseudo-pangenome (1,135 pseudo-genomes total)

These datasets are further described in :ref:`datasets <datasets>`. Memory usage and runtime were also compared to `Kmer-db <https://github.com/refresh-bio/kmer-db>`_ . Shell scripts to execute all PanKmer-only benchmarking runs and all comparative PanKmer vs Kmer-db benchmarking runs can be found in the `PanKmer GitLab repository <https://gitlab.com/salk-tm/pankmer/-/tree/main/benchmark>`_ .

All pankmer runs were parametrized to execute with 20 threads (:zsh:`--threads 20`), default gzip level (6), and a *k*-mer complete index result. In all cases except the *H. sapiens* pangenome, the input genomes were compressed. The hard-coded *k*-mer size of 31 bp was in place for all cases. For PanKmer only runs, the :zsh:`--rounds` parameter was set either to 16 for moderate memory usage with moderate runtimes or to 256 for low memory usage with long runtimes. For PanKmer vs Kmer-db runs, :zsh:`--rounds` was set to 1 for highest memory usage with shortest runtimes. All Kmer-db runs had parameters :zsh:`-k 30` to use the maximum k-mer size of 30 and either :zsh:`-t 20` or :zsh:`-t 4` to use either 20 or 4 threads.

.. Building the PanKmer index or Kmer-db database from up to thousands of eukaryote genomes is computationally intensive. For each pangenome dataset, we executed 2-5 benchmarks using differently sized subsets of the input genomes, to demonstrate the resource requirements of PanKmer at different scales. The highest peak memory consumption occurred in the full-sized *Zea* super-pangenome benchmarks:  80 GB under the moderate memory/runtime parameter setting (:zsh:`--rounds 16`), and 19 GB under the low memory / long runtime setting (:zsh:`--rounds 256`). This is due to the large number of *k*-mers (:math:`>8\times10^9`) in the *Zea* pangenome dataset. The longest runtime occurred in the full-sized H. sapiens pangenome benchmarks: 1364 minutes under moderate memory/runtime (approx. 23 hours) and 17801 minutes under low memory / long runtime (approx. 12 days). This is due to the long length of the human genomes (>3 gigabases). While 12 days is an exceptionally long time for a modern bioinformatics analysis, we note that this enabled building the PanKmer index of all 94 H. sapiens haplotypes using less than 12 GB of memory. In Kmer-db, the tradeoff of memory consumption against runtime is regulated by the number of threads. We chose two Kmer-db configurations, high memory/short runtime (:zsh:`-t 20`) and low memory/long runtime (:zsh:`-t 4`) We ran benchmarks comparing PanKmer to Kmer-db on *Typha*, *Solanum*, *H. sapiens* and *A. thaliana* datasets. In general, Kmer-db with :zsh:`-t 20` was about twice as fast as PanKmer but had a much higher memory footprint, requiring 535 GB RAM to build a database from 16 *H. sapiens* genomes, while PanKmer required 221 GB to build the same pangenome at its most memory-intensive parameter setting. With :zsh:`-t 4`, Kmer-db had roughly similar runtimes to PanKmer but still had a higher memory footprint.

**Computing environment**

The computer used for benchmarking was of the following configuration:

* 2 Intel Xeon Gold 6326 CPUs, 16 double-threaded cores per CPU, clocked at 2.9 GHz
* 1024 GB RAM
* 12 HDDs of size 2.4 TB each, hdparm -t reported buffered read speed 2317.27 MB/sec
* Ubuntu 20.04 x86-64 OS

PanKmer was compiled with Rust version 1.71.0. Kmer-db binary was installed via `bioconda <https://bioconda.github.io/recipes/kmer-db/README.html>`_ .

.. csv-table:: Sizes of pangenome datasets
   :header: "Clade","N. genomes","Mean genome size (Mb)","Total 31-mers (:math:`10^6`)"

   "*Typha*",13,222,542
   "*Solanum*",46,807,1181
   "*Zea*",54,2426,8266
   "*H. sapiens*",94,3015,3188
   "*A. thaliana*",1135,119,306

.. figure:: _static/bench-16.*
   :alt: Benchmark results with 16 rounds

   Benchmark results with 16 rounds

.. figure:: _static/bench-256.*
   :alt: Benchmark results with 256 rounds

   Benchmark results with 256 rounds

.. csv-table:: Resource usage of PanKmer indexing (v0.17.0) with 16 rounds
   :header: "Clade","N. genomes","Peak memory (GB)","Time (minutes)"

   "*Typha*",4,6,5.56
   "*Typha*",13,8,18.61
   "*Solanum*",4,14,16.26
   "*Solanum*",16,20,62.87
   "*Solanum*",46,20,215.19
   "*Zea*",4,20,50.12
   "*Zea*",16,71,239.38
   "*Zea*",54,80,783.10
   "*H. sapiens*",4,34,66.45
   "*H. sapiens*",16,35,248.50
   "*H. sapiens*",64,38,707.39
   "*H. sapiens*",94,40,1364.33
   "*A. thaliana*",4,6,2.08
   "*A. thaliana*",16,6,7.98
   "*A. thaliana*",64,7,35.90
   "*A. thaliana*",256,7,142.93
   "*A. thaliana*",1135,15,648.90

.. csv-table:: Resource usage of PanKmer indexing (v0.17.0) with 256 rounds
   :header: "Clade","N. genomes","Peak memory (GB)","Time (minutes)"

   "*Typha*",4,3,71.87
   "*Typha*",13,3,240.40
   "*Solanum*",4,5,246.94
   "*Solanum*",16,7,943.86
   "*Solanum*",46,8,2893.45
   "*Zea*",4,11,668.40
   "*Zea*",16,13,3391.95
   "*Zea*",54,19,10930.97
   "*H. sapiens*",4,7,839.12
   "*H. sapiens*",16,7,3015.26
   "*H. sapiens*",64,11,11268.46
   "*H. sapiens*",94,11,17800.85
   "*A. thaliana*",4,6,28.28
   "*A. thaliana*",16,6,113.29
   "*A. thaliana*",64,6,498.88
   "*A. thaliana*",256,7,2000.42
   "*A. thaliana*",1135,8,9425.92

.. figure:: _static/pankmer-bench-heatmaps.png
   :alt: Kmer-db vs PanKmer runtime

   Heatmaps of PanKmer benchmarking pangenome datasets. A) *Solanum* B) *Zea* C) *H. sapiens* D) *A. thaliana*


PanKmer vs Kmer-db
------------------

.. figure:: _static/bench-pk-kdb-time.*
   :alt: Kmer-db vs PanKmer runtime

   Kmer-db vs PanKmer runtime

.. figure:: _static/bench-pk-kdb-mem.*
   :alt: Kmer-db vs PanKmer peak memory usage

   Kmer-db vs PanKmer peak memory usage

.. csv-table:: Resource usage of Kmer-db (20 threads)
   :header: "Clade","N. genomes","Peak memory (GB)","Time (minutes)"

   "*Typha*",4,72,3.45
   "*Typha*",13,88,5.15
   "*Solanum*",4,110,6.40
   "*Solanum*",16,186,10.17
   "*H. sapiens*",4,281,16.17
   "*H. sapiens*",16,535,32.65
   "*A. thaliana*",64,81,3.59
   "*A. thaliana*",256,89,9.88
   "*A. thaliana*",1135,107,36.73

.. csv-table:: Resource usage of Kmer-db (4 threads)
   :header: "Clade","N. genomes","Peak memory (GB)","Time (minutes)"

   "*Typha*",4,70,3.45
   "*Typha*",13,74,6.28
   "*Solanum*",4,105,7.80
   "*Solanum*",16,119,16.73
   "*H. sapiens*",4,266,17.08
   "*H. sapiens*",16,322,53.25
   "*A. thaliana*",64,65,7.65
   "*A. thaliana*",256,73,23.78
   "*A. thaliana*",1135,85,102.87

.. csv-table:: Resource usage of PanKmer (1 round, 20 threads)
   :header: "Clade","N. genomes","Peak memory (GB)","Time (minutes)"

   "*Typha*",4,28,1.95
   "*Typha*",13,40,4.10
   "*Solanum*",4,61,11.67
   "*Solanum*",16,82,21.76
   "*H. sapiens*",4,221,20.85
   "*H. sapiens*",16,221,55.32
   "*A. thaliana*",64,13,4.49
   "*A. thaliana*",256,21,17.72
   "*A. thaliana*",1135,60,85.13

Appendix: details of memory block scheme
----------------------------------------

A basic action of PanKmer is to read k-mers from a genome into memory

.. figure:: _static/kmer-counting-simple.*
   :alt: simple k-mer counting

   Simple k-mer counting

However, doing this naively results in excesive memory usage

.. figure:: _static/kmer-counting-overmem.*
   :alt: k-mer counting memory overrun

   Memory overrun

As a solution, PanKmer breaks up the space of k-mers into blocks and operates
on them sequentially. This limits the peak memory requirement. The number of
blocks is determined by the :zsh:`--rounds` argument.

.. figure:: _static/kmer-counting-blocks.*
   :alt: k-mer counting with memory blocks

   K-mer counting with memory blocks

Adding multiple genomes and scores fits naturally into this scheme

.. figure:: _static/kmer-counting-scores-blocks.*
   :alt: k-mer counting with blocks and scores

   K-mer counting with memory blocks and scores
