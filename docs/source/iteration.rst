Iterating over the index
========================

.. role:: zsh(code)
   :language: zsh

Iterating via API
-----------------

PanKmer's python API provides tools to iterate over the *k*-mers and scores of an index and decode them from binary format. The following Python script :zsh:`pk_iter.py` illustrates the use of the API to iterate over *k*-mers and scores of an index.

.. code-block:: python

   # pk_iter.py
   from argparse import ArgumentParser
   from itertools import islice
   from pankmer import PKResults

   def parse_arguments():
       parser = ArgumentParser()
       parser.add_argument('index')
       return parser.parse_args()
   
   args = parse_arguments()
   pkr = PKResults(args.index)
   for kmer_b, score_b in islice(pkr, 5):
       print(kmer_b, score_b)
   for kmer_b, score_b in islice(pkr, 5):
       kmer, score = pkr.decode_kmer_str(kmer_b), pkr.decode_score(score_b)
       print(kmer, score)

This script iterates over the index twice. First it reads 5 *k*-mer, score pairs and prints them as Python bytes objects. Second, it reads the same 5 *k*-mer, score pairs, and decodes the bytes objects to a string and a list, respectively. See below an example usage of :zsh:`pk_iter.py`:

.. code-block:: none

   mkdir bsub
   pankmer download-example -n 3 -c Bsubtilis -d bsub
   pankmer index -k 15 -g bsub -o bsub-idx.tar
   python pk_iter.py bsub-idx.tar

.. code-block:: none

   b'\x00\x00\xbf\xff' b'\x07'
   b'\x00\x00\xff\xff' b'\x04'
   b'\x00\x01?\xff' b'\x07'
   b'\x00\x02o\xff' b'\x05'
   b'\x00\x02\xdf\xff' b'\x01'
   TTTTTTTCAAAAAAA [True, True, True]
   TTTTTTTAAAAAAAA [False, False, True]
   TTTTTTGTAAAAAAA [True, True, True]
   TTTTTTCGCAAAAAA [True, False, True]
   TTTTTTCAGAAAAAA [True, False, False]


Reformatting the index
----------------------

PanKmer stores index data in a highly compressed binary format. If you have adequate disk space, you may find it convenient to expand the *k*-mers and/or scores to a text format for some other application. The following examples show how to use the :zsh:`subset`: subcommand and the :zsh:`reformat-index`: subcommand to extract scores and *k*-mers.

First create your index.

.. code-block:: none

   mkdir bsub
   pankmer download-example -n 5 -c Bsubtilis -d bsub
   pankmer index -k 15 -g bsub -o bsub-idx.tar

Use :zsh:`subset`: to reduce the search to a single or small set of genomes.

.. code-block:: none
    
    pankmer subset -i bsub-idx.tar -o bsub-idx.subset.tar -g GCF_000772125.1_ASM77212v1_genomic GCF_000772165.1_ASM77216v1_genomic

Then use :zsh:`reformat-index`: to extract the scores and *k*-mers.

.. code-block:: none

   pankmer reformat-index -i bsub-idx.subset.tar -o expanded-scores.bgz -t scores
   pankmer reformat-index -i bsub-idx.subset.tar -o expanded-kmers.bgz -t kmers
   gunzip -c expanded-scores.bgz | head; gunzip -c expanded-kmers.bgz | head

.. code-block:: none

   11
   11
   10
   10
   11
   11
   11
   11
   01
   10
   TTTTTTTCAAAAAAA
   TTTTTTGTAAAAAAA
   TTTTTTCGCAAAAAA
   TTTTTTCAGAAAAAA
   TTTTTTCACAAAAAA
   TTTTTTCAAAAAAAA
   TTTTTTATGAAAAAA
   TTTTTTATCAAAAAA
   TTTTTTATAAAAAAA
   TTTTTTACGAAAAAA

Example: Unique Kmer Sequences
------------------------------

Using the expanded-scores.bgz, expanded-kmers.bgz, and metadata.json, you may be able to extract the unique kmers using a Python script. If you have adequate disk space, you may be able to use :zsh:`reformat-index`: to extract the unique *k*-mers not shared between your index genomes.

.. code-block:: none

   pankmer reformat-index -i bsub-idx.subset.tar -o unique-kmers.tsv -t unique
   head unique-kmers.tsv

.. code-block:: none

   GCF_000772125.1_ASM77212v1_genomic      GCF_000772165.1_ASM77216v1_genomic
   TTTTTTCGCAAAAAA TTTTTTATAAAAAAA
   TTTTTTCAGAAAAAA TTTTTGTGAAAAAAA
   TTTTTTACGAAAAAA TTTTTGTAGAAAAAA
   TTTTTGCACCAAAAA TTTTTGGAGAAAAAA
   TTTTTGATTAAAAAA TTTTTGGACAAAAAA
   TTTTTGAATCAAAAA TTTTTGCTGAAAAAA
   TTTTTGAAGAAAAAA TTTTTGCGAAAAAAA
   TTTTTCTTAAAAAAA TTTTTGCAGAAAAAA
   TTTTTCTGTAAAAAA TTTTTGATGAAAAAA