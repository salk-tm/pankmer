Dependencies
============

Pending shipment of binaries, installing PanKmer requires the `Rust <https://www.rust-lang.org/>`_ programming language and its crate manager `Cargo <https://doc.rust-lang.org/cargo/>`_.

PanKmer also depends on the following Python packages: biopython seaborn urllib3 newick pyfaidx gff2bed upsetplot pybedtools cython more-itertools
