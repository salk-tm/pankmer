API reference
=====================

.. autoclass:: pankmer.PKResults
  :members:

.. autofunction:: pankmer.index_wrapper

.. autofunction:: pankmer.get_adjacency_matrix

.. autofunction:: pankmer.clustermap

.. autofunction:: pankmer.anchor_region

.. autofunction:: pankmer.anchor_genome