Raw sequencing reads
====================

.. role:: zsh(code)
   :language: zsh

In addition to assemblies in FASTA format, PanKmer can also operate on raw sequencing reads in FASTQ format. They are handled differently to account for the possibility of sequencing errors.

When FASTQ files are included as input genomes, the indexing process is altered so that, during indexing, each k-mer's score is composed of 2 bits per genome instead of 1 bit. This increases the memory footprint but allows tracking whether each k-mer has been observed one, 2, or >2 times in each genome.

It is recommended to preprocess FASTQ data before indexing them with PanKmer. Consider using a preprocessing tool such as `fastp <https://github.com/OpenGene/fastp>`_.
