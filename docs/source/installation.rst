Installation
============

.. role:: zsh(code)
   :language: zsh

In a conda environment
----------------------

First create an environment that includes all dependencies:

.. code-block:: zsh

   conda create -c conda-forge -c bioconda -n pankmer \
     python=3.10 cython gff2bed more-itertools pybedtools \
     python-newick pyfaidx rust seaborn upsetplot urllib3 \
     tabix dash-bootstrap-components

Then install PanKmer with :zsh:`pip`:

.. code-block:: zsh

   conda activate pankmer
   pip install pankmer

Alternatively, to install from source:

.. code-block:: zsh

   conda activate pankmer
   git clone https://gitlab.com/salk-tm/pankmer.git
   cd pankmer
   pip install .

With pip
--------

PanKmer is built with `Rust <https://doc.rust-lang.org/stable/book/title-page.html>`_ ,
so you will need to `install <https://doc.rust-lang.org/stable/book/ch01-01-installation.html>`_
it if you have not already done so. Then you can install PanKmer with :zsh:`pip`:

.. code-block:: zsh

   pip install pankmer

Check installation
------------------

Check that the installation was successful by running:

.. code-block:: none

   pankmer --version

See the available subcommands by running :zsh:`pankmer --help`:

.. code-block:: none

   usage: pankmer [-h] [--version]
                  {index,count,collect,upset,subset,adj-matrix,tree,clustermap,similarity,distance,anchor-region,anchor-genome,anchor-plot,anchor-heatmap,anchormap,dryrun,download-example}
                  ...

   positional arguments:
     {index,count,collect,upset,subset,adj-matrix,tree,clustermap,similarity,distance,anchor-region,anchor-genome,anchor-plot,anchor-heatmap,anchormap,dryrun,download-example}
       index               generate k-mer index
       count               count k-mers in one or more indexes
       collect             calculate k-mer collection curve
       upset               generate upset plot
       subset              subset an index
       adj-matrix          generate adjacency matrix
       tree                generate a heirarchical clustering tree from an adjacency matrix
       clustermap          plot a clustered heatmap from the adjacency matrix
       similarity          generate a similarity matrix from an adjacency matrix
       distance            generate a distance matrix from an adjacency matrix
       anchor-region       anchor k-mers in a region
       anchor-genome       anchor k-mers in a genome
       anchor-plot         generate a plot from genome anchoring results
       anchor-heatmap      draw anchor heatmap
       anchormap           export an anchormap for downstream visualization
       dryrun              perform a dry run of indexing and print metadata
       download-example    download an example dataset
