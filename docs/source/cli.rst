CLI reference
=============

.. role:: zsh(code)
   :language: zsh

:zsh:`index`
------------

.. code-block::  none

   usage: pankmer index [-h] [-g <genome[s]{.fa,.fq,.tar,/}> [<genome[s]{.fa,.fq,.tar,/}> ...]]
                        [-p <genome[s]{.fa,.fq,/}> [<genome[s]{.fa,.fq,/}> ...]] -o <output[.tar]> [--split-memory <int>]
                        [-t <int>] [--fraction <float> | -q <int>] [--gzip-level {1,2,3,4,5,6,7,8,9}] [--time]

   options:
     -h, --help            show this help message and exit
     -g <genome[s]{.fa,.fq,.tar,/}> [<genome[s]{.fa,.fq,.tar,/}> ...], --genomes <genome[s]{.fa,.fq,.tar,/}> [<genome[s]{.fa,.fq,.tar,/}> ...]
                           paths to input genomes, directories, or tar archive
     -p <genome[s]{.fa,.fq,/}> [<genome[s]{.fa,.fq,/}> ...], --genomes-paired <genome[s]{.fa,.fq,/}> [<genome[s]{.fa,.fq,/}> ...]
                           paths to input genomes or directories (paired)
     -o <output[.tar]>, --output <output[.tar]>
                           output directory or tarfile that will contain the k-mer index
     --rounds <int>        split indexing into multiple rounds to reduce memory usage
     -t <int>, --threads <int>
                           Number of threads to use [1]
     --fraction <float>    Fraction of k-mers to use. By default all k-mers are kept.
     -q <int>, --qual <int>
                           Completeness of index, in terms of bases, expressed as a Phred quality score. e.g. --qual 30
                           means approx 1 in 1000 bases will be missed, which is equivalent to --fraction 0.2. By default
                           no bases are missed.
     --gzip-level {1,2,3,4,5,6,7,8,9}
                           gzip compression level [6]
     --time                Report the time required to execute


:zsh:`count`
------------

.. code-block:: none

   usage: pankmer count [-h] -i <index[.tar]> [<index[.tar]> ...]

   options:
     -h, --help            show this help message and exit
     -i <index[.tar]> [<index[.tar]> ...], --index <index[.tar]> [<index[.tar]> ...]
                           a k-mer index

:zsh:`collect`
--------------

.. code-block:: none

   usage: pankmer collect [-h] -i <index[.tar]> [-o <output.{pdf,png,svg}>] [-t <output-table.tsv>] [--title <"Plot title">]
                          [--width <float>] [--height <float>] [--color-palette <#color> [<#color> ...]] [--alpha <float>]
                          [--linewidth <int>] [--conf | --contours <int> [<int> ...]]
                          [--legend-loc {best,upper left,upper right,lower left,lower right,outside}]

   options:
     -h, --help            show this help message and exit
     -i <index[.tar]>, --index <index[.tar]>
                           a k-mer index
     -o <output.{pdf,png,svg}>, --output <output.{pdf,png,svg}>
                           destination file for plot
     -t <output-table.tsv>, --table <output-table.tsv>
                           output TSV file containing plotted data
     --title <"Plot title">
                           set the title for the plot
     --width <float>       set width of figure in inches [4]
     --height <float>      set height of figure in inches [3]
     --color-palette <#color> [<#color> ...]
                           color palette to use
     --alpha <float>       transparency value for lines [1.0]
     --linewidth <int>     line width for plot [3]
     --conf                calculate confidence intervals for collection curves
     --contours <int> [<int> ...]
                           set contours for collection curves (in percent)
     --legend-loc {best,upper left,upper right,lower left,lower right,outside}
                           location of legend [best]


:zsh:`upset`
------------

.. code-block:: none

   usage: pankmer upset [-h] -i <index[.tar]> -o <output.{pdf,png,svg}> -g <genome> [<genome> ...] [-v] [-x] [-t <table.tsv[.gz]>]
                        [--show-counts] [--min-subset-size <int>] [--max-subset-size <int>] [--time]

   options:
     -h, --help            show this help message and exit
     -i <index[.tar]>, --input <index[.tar]>
                           a k-mer index
     -o <output.{pdf,png,svg}>, --output <output.{pdf,png,svg}>
                           destination file for upset plot
     -g <genome> [<genome> ...], --genomes <genome> [<genome> ...]
                           list of genomes to include
     -v, --vertical        draw the plot vertically
     -x, --exclusive       exclude k-mers that occur in genomes other than the input set
     -t <table.tsv[.gz]>, --table <table.tsv[.gz]>
                           write (optionally compressed) table of values in tsv format
     --show-counts         show counts for each subset
     --min-subset-size <int>
                           show only subsets larger than a minimum size
     --max-subset-size <int>
                           show only subsets smaller than a maximum size
     --time                report the time required to execute

:zsh:`subset`
-------------

.. code-block:: none

   usage: pankmer subset [-h] -i <input-index[.tar]> -o <output-index[.tar]> -g <genome> [<genome> ...] [-x]
                         [--gzip-level {1,2,3,4,5,6,7,8,9}] [--time]

   options:
     -h, --help            show this help message and exit
     -i <input-index[.tar]>, --input <input-index[.tar]>
                           a k-mer index
     -o <output-index[.tar]>, --output <output-index[.tar]>
                           destination file for subset index
     -g <genome> [<genome> ...], --genomes <genome> [<genome> ...]
                           list of genomes to include
     -x, --exclusive       exclude k-mers that occur in genomes other than the input set
     --gzip-level {1,2,3,4,5,6,7,8,9}
                           gzip compression level [6]
     --time                report the time required to execute

:zsh:`adj-matrix`
-----------------

.. code-block::  none

   usage: pankmer adj-matrix [-h] -i <index[.tar]> -o <adjmatrix.{csv,tsv}> [--time]

   options:
     -h, --help            show this help message and exit
     -i <index[.tar]>, --input <index[.tar]>
                           a k-mer index
     -o <adjmatrix.{csv,tsv}>, --output <adjmatrix.{csv,tsv}>
                           destination file for adjacency matrix
     --time                Report the time required to execute


:zsh:`tree`
-----------

.. code-block::  none

   usage: pankmer tree [-h] -i <adjmatrix.{csv,tsv}> [-n] [--metric {intersection,jaccard,overlap,qv,qv_symmetric,ani}]
                       [--method {single,complete,average,weighted,centroid}] [--transformed-matrix <matrix.{csv,tsv}>]

   options:
     -h, --help            show this help message and exit
     -i <adjmatrix.{csv,tsv}>, --input <adjmatrix.{csv,tsv}>
                           adjacency matrix file
     -n, --newick          output tree in NEWICK format
     --metric {intersection,jaccard,overlap,qv,qv_symmetric,ani}
                           similarity metric [intersection]
     --method {single,complete,average,weighted,centroid}
                           clustering method [complete]
     --transformed-matrix <matrix.{csv,tsv}>
                           Write similarity transformed matrix to file


:zsh:`clustermap`
-----------------

.. code-block:: none

   usage: pankmer clustermap [-h] -i <adjmatrix.{csv,tsv}> -o <adjmatrix.{pdf,png,svg}>
                             [--metric {intersection,jaccard,overlap,qv,qv_symmetric,ani}]
                             [--method {single,complete,average,weighted,centroid}] [--colormap <color_map>] [--width <float>]
                             [--height <float>] [--heatmap-ticks {left,right}] [--cbar-ticks {left,right}] [--dend-ratio <float>]
                             [--dend-spacer <float>]

   options:
     -h, --help            show this help message and exit
     -i <adjmatrix.{csv,tsv}>, --input <adjmatrix.{csv,tsv}>
                           adjacency matrix file
     -o <adjmatrix.{pdf,png,svg}>, --output <adjmatrix.{pdf,png,svg}>
                           destination file for plot
     --metric {intersection,jaccard,overlap,qv,qv_symmetric,ani}
                           similarity metric [intersection]
     --method {single,complete,average,weighted,centroid}
                           clustering method [complete]
     --colormap <color_map>
                           seaborn colormap for plot [mako_r]
     --width <float>       width of plot in inches [7]
     --height <float>      height of plot in inches [7]
     --heatmap-ticks {left,right}
                           Position of heatmap ticks. Must be "left" or "right" [left]
     --cbar-ticks {left,right}
                           Position of color bar ticks. Must be "left" or "right" [left]
     --dend-ratio <float>  Fraction of plot width used for dendrogram [0.2]
     --dend-spacer <float>
                           Fraction of plot width used as spacer between dendrogram and heatmap [0.1]

:zsh:`similarity`
-----------------

.. code-block::  none

   usage: pankmer similarity [-h] -i <adjmatrix.{csv,tsv}> -o <simmatrix.{csv,tsv}>
                             [--metric {jaccard,overlap,qv,qv_symmetric,ani}]

   options:
     -h, --help            show this help message and exit
     -i <adjmatrix.{csv,tsv}>, --input <adjmatrix.{csv,tsv}>
                           adjacency matrix file
     -o <simmatrix.{csv,tsv}>, --output <simmatrix.{csv,tsv}>
                           similarity matrix file
     --metric {jaccard,overlap,qv,qv_symmetric,ani}
                           similarity metric [jaccard]


:zsh:`distance`
---------------

.. code-block:: none

   usage: pankmer distance [-h] -i <adjmatrix.{csv,tsv}> -o <distmatrix.{csv,tsv}> [--metric {jaccard,overlap,ani}]

   options:
     -h, --help            show this help message and exit
     -i <adjmatrix.{csv,tsv}>, --input <adjmatrix.{csv,tsv}>
                           adjacency matrix file
     -o <distmatrix.{csv,tsv}>, --output <distmatrix.{csv,tsv}>
                           distance matrix file
     --metric {jaccard,overlap,ani}
                           distance metric [jaccard]


:zsh:`anchor-region`
--------------------

.. code-block:: none

   usage: pankmer anchor-region [-h] -i <index[.tar]> [<index[.tar]> ...] -r <reference.fa.gz> -c <chr:start-end>
                                [-o <output.bdg[.gz]>] [-b] [-g <genes.gff3[.gz]>] [-f <int>] [-p <int>]

   options:
     -h, --help            show this help message and exit
     -i <index[.tar]> [<index[.tar]> ...], --index <index[.tar]> [<index[.tar]> ...]
                           index
     -r <reference.fa.gz>, --reference <reference.fa.gz>
                           reference
     -c <chr:start-end>, --coords <chr:start-end>
                           genomic coordinates
     -o <output.bdg[.gz]>, --output <output.bdg[.gz]>
                           write to file instead of standard output
     -b, --bgzip           block compress the output file
     -g <genes.gff3[.gz]>, --genes <genes.gff3[.gz]>
                           gff file of gene coordinates
     -f <int>, --flank <int>
                           size of flanking regions
     -p <int>, --processes <int>
                           number of processes to use

:zsh:`anchor-genome`
--------------------

.. code-block:: none

   usage: pankmer anchor-genome [-h] -i <index[.tar]> [<index[.tar]> ...] -o <output.{pdf,png,svg}> -r <reference.fa.gz> -c <chrX>
                                [<chrX> ...] [-t <output-table.tsv>] [--groups <"Group"> [<"Group"> ...]] [--title <"Plot title">]
                                [--x-label <"Label">] [--legend] [--legend-title <"Title">]
                                [--legend-loc {best,upper left,upper right,lower left,lower right,outside}] [--bin-size <int>]
                                [--width <float>] [--height <float>] [--color-palette <#color> [<#color> ...]] [--alpha <float>]
                                [--linewidth <int>] [--processes <int>]

   options:
     -h, --help            show this help message and exit
     -i <index[.tar]> [<index[.tar]> ...], --index <index[.tar]> [<index[.tar]> ...]
                           index
     -o <output.{pdf,png,svg}>, --output <output.{pdf,png,svg}>
                           destination file for plot
     -r <reference.fa.gz>, --reference <reference.fa.gz>
                           reference in BGZIP compressed FASTA format
     -c <chrX> [<chrX> ...], --chromosomes <chrX> [<chrX> ...]
                           chromosomes to include
     -t <output-table.tsv>, --table <output-table.tsv>
                           output TSV file containing plotted data
     --groups <"Group"> [<"Group"> ...]
                           list of groups for provided indexes [0]
     --title <"Plot title">
                           set the title for the plot
     --x-label <"Label">   set the x-axis label for the plot
     --legend              include a legend with the plot
     --legend-title <"Title">
                           title of legend
     --legend-loc {best,upper left,upper right,lower left,lower right,outside}
                           location of legend [best]
     --bin-size <int>      Set bin size. The input <int> is converted to the bin size by the formula: 10^(<int>+6) bp. The default
                           value is 0, i.e. 1-megabase bins. [0]
     --width <float>       set width of figure in inches [7]
     --height <float>      set height of figure in inches [3]
     --color-palette <#color> [<#color> ...]
                           color palette to use
     --alpha <float>       transparency value for lines [0.5]
     --linewidth <int>     line width for plot [3]
     --processes <int>     number of processes to use


:zsh:`anchor-plot`
------------------

.. code-block:: none

   usage: pankmer anchor-plot [-h] -t <genomecov.tsv> -o <output.{pdf,png,svg}> [--groups <"Group"> [<"Group"> ...]]
                              [--loci <"chr:pos:name"> [<"chr:pos:name"> ...]] [--chromsizes <file.chrom.sizes>]
                              [--title <"Plot title">] [--x-label <"Label">] [--legend] [--legend-title <"Title">]
                              [--legend-loc {best,upper left,upper right,lower left,lower right,outside}] [--width <float>]
                              [--height <float>] [--color-palette <#color> [<#color> ...]] [--alpha <float>] [--linewidth <int>]

   options:
     -h, --help            show this help message and exit
     -t <genomecov.tsv>, --table <genomecov.tsv>
                           genomic coverage results
     -o <output.{pdf,png,svg}>, --output <output.{pdf,png,svg}>
                           destination file for plot
     --groups <"Group"> [<"Group"> ...]
                           list of groups for provided indexes
     --loci <"chr:pos:name"> [<"chr:pos:name"> ...]
                           list of loci to mark on plot
     --chromsizes <file.chrom.sizes>
                           chromsizes file of the reference used to generate the table
     --title <"Plot title">
                           set the title for the plot
     --x-label <"Label">   set x-axis label for the plot
     --legend              include a legend with the plot
     --legend-title <"Title">
                           title of legend
     --legend-loc {best,upper left,upper right,lower left,lower right,outside}
                           location of legend [best]
     --width <float>       set width of figure in inches [7]
     --height <float>      set height of figure in inches [3]
     --color-palette <#color> [<#color> ...]
                           color palette to use
     --alpha <float>       transparency value for lines [0.5]
     --linewidth <int>     line width for plot [3]

:zsh:`dryrun`
-------------

.. code-block:: none

   usage: pankmer dryrun [-h] [-g <genome[s]{.fa,.fq,.tar,/}> [<genome[s]{.fa,.fq,.tar,/}> ...]]
                         [-p <genome[s]{.fa,.fq,.tar,/}> [<genome[s]{.fa,.fq,.tar,/}> ...]] [--split-memory <int>] [-t <int>]

   options:
     -h, --help            show this help message and exit
     -g <genome[s]{.fa,.fq,.tar,/}> [<genome[s]{.fa,.fq,.tar,/}> ...], --genomes <genome[s]{.fa,.fq,.tar,/}> [<genome[s]{.fa,.fq,.tar,/}> ...]
                           input genomes
     -p <genome[s]{.fa,.fq,/}> [<genome[s]{.fa,.fq,/}> ...], --genomes-paired <genome[s]{.fa,.fq,/}> [<genome[s]{.fa,.fq,/}> ...]
                           input genomes (paired)
     --rounds <int>        Parallel.This splits the indexing into multiple rounds; reducing memory.
     -t <int>, --threads <int>
                           Number of threads to use

:zsh:`download-example`
-----------------------

.. code-block:: none

   usage: pankmer download-example [-h] [-d <dir/>] [-c {Spolyrhiza,Solanum,Zea,Hsapiens,Bsubtilis,Athaliana}] [-n <int>]

   options:
     -h, --help            show this help message and exit
     -d <dir/>, --dir <dir/>
                           destination directory for example data
     -c {Spolyrhiza,Solanum,Zea,Hsapiens,Bsubtilis,Athaliana}, --clade {Spolyrhiza,Solanum,Zea,Hsapiens,Bsubtilis,Athaliana}
                           download publicly available genomes. Clade: max_samples. Spolyrhiza: 3, Solanum: 46, Zea: 54, Hsapiens:
                           94, Bsubtilis: 164, Athaliana: 1135
     -n <int>, --n-samples <int>
                           number of samples to download, must be less than clade max [1]
