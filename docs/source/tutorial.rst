Tutorial
========

.. role:: zsh(code)
   :language: zsh

.. _download_example:

Download example dataset
------------------------

The :zsh:`download-example` subcommand will download a small example dataset of
Chr19 sequences from *S. polyrhiza* [#f1]_ .


.. code-block:: zsh

   pankmer download-example -d .

After running this command the directory :zsh:`PanKmer_example_Sp_Chr19/` will be present in the working directory. It contains FASTA files representing Chr19 from three genomes, and GFF files giving their gene annotations.

.. code-block:: zsh

   ls PanKmer_example_Sp_Chr19/*

.. code-block:: zsh

   PanKmer_example_Sp_Chr19/README.md

   PanKmer_example_Sp_Chr19/Sp_Chr19_features:
   Sp9509_oxford_v3_Chr19.gff3.gz Sp9512_a02_genes_Chr19.gff3.gz

   PanKmer_example_Sp_Chr19/Sp_Chr19_genomes:
   Sp7498_HiC_Chr19.fasta.gz Sp9509_oxford_v3_Chr19.fasta.gz Sp9512_a02_genome_Chr19.fasta.gz

.. note::

   GFF files are included in the example data, but are not required for basic pangenome analysis and are not used in this tutorial. Examples using GFF data can be found in the :ref:`examples <genome_anchoring>` section.

To get started, navigate to the downloaded directory.

.. code-block:: zsh

   cd PanKmer_example_Sp_Chr19/

.. _kmer_index:

Build a *k*-mer index
---------------------

The *k*-mer index is a table tracking presence or absence of *k*-mers in the set
of input genomes. To build an index, use the :zsh:`index` subcommand and provide a directory containing the input genomes.

.. code-block:: zsh

   pankmer index -g Sp_Chr19_genomes/ -o Sp_Chr19_index.tar

After completion, the index will be present as a tar file :zsh:`Sp_Chr19_index.tar`.

.. code-block:: zsh

   tar -tvf Sp_Chr19_index.tar

.. code-block:: none

   Sp_Chr19_index/
   Sp_Chr19_index/kmers.b.gz
   Sp_Chr19_index/metadata.json
   Sp_Chr19_index/scores.b.gz

.. note::

   The input genomes argument provided with the :zsh:`-g` flag can be a directory,
   a tar archive, or a space-separated list of FASTA files.

   If the output argument provided with the :zsh:`-o` flag ends with :zsh:`.tar`,
   then the index will be written as a tar archive. Otherwise it will be written
   as a directory.

   The default *k*-mer size *k* is 31. The :zsh:`-k` / :zsh:`--kmer-size` option can
   be used to set a different value for *k*, which may be any odd integer
   between 9 and 31.

Create an adjacency matrix
--------------------------

A useful application of the *k*-mer index is to generate an adjacency matrix. This is a table of *k*-mer similarity values for each pair of genomes in the index. We can generate one using the :zsh:`adj-matrix` subcommand, which will produce a CSV or TSV file containing the matrix.

.. code-block:: zsh

   pankmer adj-matrix -i Sp_Chr19_index.tar -o Sp_Chr19_adj_matrix.csv
   pankmer adj-matrix -i Sp_Chr19_index.tar -o Sp_Chr19_adj_matrix.tsv

.. note::

   The input index argument provided with the :zsh:`-i` flag can be tar archive
   or a directory.

Plot a clustered heatmap
------------------------

To visualize the adjacency matrix, we can plot a clustered heatmap of the adjacency values. In this case we use the default Average Nucleotide Identity metric for pairwise comparisons between genomes:

.. code-block:: zsh

   pankmer clustermap -i Sp_Chr19_adj_matrix.csv \
     -o Sp_Chr19_adj_matrix.svg

.. figure:: _static/Sp_Chr19_adj_matrix.*
   :alt: Clustered heatmap generated from *S. polyrhiza* adjacency matrix.

   Clustered heatmap generated from *S. polyrhiza* adjacency matrix.

.. [#f1] Harkess A. et al. Improved Spirodela polyrhiza genome and proteomic analyses reveal a conserved chromosomal structure with high abundance of chloroplastic proteins favoring energy production. *Journal of Experimental Botany* 2021
