Tests
=====

.. role:: zsh(code)
   :language: zsh

To run PanKmer's unit tests, download the git repository and use `pytest <https://docs.pytest.org/en/>`_. This assumes you have a conda environment set up as in :doc:`installation`:

.. code-block:: zsh

   conda activate pankmer
   conda install -c conda-forge pytest fq
   git clone https://gitlab.com/salk-tm/pankmer.git
   cd pankmer
   pip install .
   pytest

The unit tests are divided into three categories: fast, slow, and very slow.
The fast tests have a short runtime and do not require an internet connection.
The slow tests have more complete coverage of PanKmer's features, but require
an internet connection to download the example dataset and use it as input. The
very slow tests have the same requirements as the slow tests but are even
slower.

By default, only the fast tests are run. You can run the slow tests by adding
the :zsh:`--runslow` option:

.. code-block:: zsh

   pytest --runslow

To run the slow and very slow tests, use the :zsh:`--veryslow` option:

.. code-block:: zsh

   pytest --veryslow
