.. _datasets:

Pangenome datasets
==================

.. role:: zsh(code)
   :language: zsh

The :zsh:`pankmer download-example` subcommand can be used to download genomes from several publicly available pangenome datasets. See the help text:

.. code-block:: zsh

   pankmer download-example --help

.. code-block:: none

usage: pankmer download-example [-h] [-d <dir/>]
                                [-c {Spolyrhiza,Lemnaceae,Solanum,Zea,Hsapiens,Bsubtilis,Athaliana}] [-n <int>]

options:
  -h, --help            show this help message and exit
  -a, --annotation      download annotations instead of genome sequences
  -d <dir/>, --dir <dir/>
                        destination directory for example data
  -c {Spolyrhiza,Lemnaceae,Solanum,Zea,Hsapiens,Bsubtilis,Athaliana}, --clade {Spolyrhiza,Lemnaceae,Solanum,Zea,Hsapiens,Bsubtilis,Athaliana}
                        download publicly available genomes. Clade: max_samples. Spolyrhiza: 3,
                        Lemnaceae: 9, Solanum: 46, Zea: 53, Hsapiens: 94, Bsubtilis: 164, Athaliana: 1135
  -n <int>, --n-samples <int>
                        number of samples to download, must be less than clade max [1]

The :zsh:`-c/--clade` option selects the clade, and the :zsh:`-n/--n-samples` option selects the number of samples to download. The maximum number of samples for each clade is:

.. csv-table:: Pangenome datasets
   :header: "Clade","Max samples"

   "S. polyrhiza",3
   "Lemnaceae",9
   "Solanum",46
   "Zea",53
   "H. sapiens",94
   "B. subtilis",164
   "A. thaliana",1135

See below a description of each pangenome dataset

Lemnaceae
---------

9 Lemnaceae genomes from the  `lemna.org <https://www.lemna.org/>`_ database. See also preprint: `The genomes and epigenomes of aquatic plants (Lemnaceae) promote triploid hybridization and clonal reproduction <https://www.biorxiv.org/content/10.1101/2023.08.02.551673v2>`_ .

*Solanum*
---------

46 *Solanum* genomes from the `SolOmics database <http://solomics.agis.org.cn/tomato/ftp>`_. See also *Nature* article: `Graph pangenome captures missing heritability and empowers tomato breeding <https://www.nature.com/articles/s41586-022-04808-9>`_ .

*Zea*
-----

53 *Zea mays* and relative genomes from the `downloads page <https://download.maizegdb.org/>`_ of `MaizeGDB <https://maizegdb.org/>`_. 

*H. sapiens*
------------

94 *Homo sapiens* haplotypes from Year 1 of the `Human Pangenome Reference Consortium <https://humanpangenome.org/>`_ / `Human Pangenome Project <https://humanpangenomeproject.org/>`_. Download details found at the `HPRC/HPP github repository <https://github.com/human-pangenomics/HPP_Year1_Assemblies>`_. See also *Nature* articles: `A draft human pangenome reference <https://www.nature.com/articles/s41586-023-05896-x>`_ , `The Human Pangenome Project: a global resource to map genomic diversity <https://www.nature.com/articles/s41586-022-04601-8>`_.

*B. subtilis*
-------------

164 *B. subtilis* genomes from NCBI.

*A. thaliana*
-------------

1135 *Arabidopsis thaliana* pseudo-genomes from the `data center <https://1001genomes.org/data/GMI-MPI/releases/v3.1/pseudogenomes/fasta/>`_ of `1001 Genomes <https://1001genomes.org/index.html>`_. See also *Cell* article: `1,135 Genomes Reveal the Global Pattern of Polymorphism in Arabidopsis thaliana <https://www.cell.com/cell/fulltext/S0092-8674(16)30667-5>`_ .

*S. polyrhiza*
--------------

A collection of 3 *Spirodela polyrhiza* clones Sp7498, Sp9509, Sp9512, from the following sources: Sp7498 and Sp9509 sequences were sourced from the following references found at `http://spirodelagenome.org <http://spirodelagenome.org>`_:

| Sp9509_oxford_v3
| NCBI: GCA_900492545.1
| CoGe: id51364
| This genome was generated with Oxford Nanopore and polished with Illumina, scaffolded against the previous Illumina-based genome Sp9509v3 and validated with BioNano optical maps and multi-color FISH (mcFISH).
| 
| Hoang PNT, Michael TP, Gilbert S, Chu P, Motley TS, Appenroth KJ, Schubert I, Lam E. Generating a high-confidence reference genome map of the Greater Duckweed by integration of cytogenomic, optical mapping and Oxford Nanopore technologies. Plant J. 2018 Jul 28.
| 
| Sp7498_HiC
| CoGe: 55877
| This assembly was generated using Oxford Nanopore long reads and Illumina-based HiC scaffolding.
| 
| Harkess A, McGlaughlin F, Bilkey N, Elliott K, Emenecker R, Mattoon E, Miller K, Vierstra R, Meyers BC, Michael TP. High contiguity Spirodela polyrhiza genomes reveal conserved chromosomal structure. Submitted.
| 
| 
| Sp9512 sequence was sourced from research data for the following in-progress publication:
| 
| 
| Pasaribu B, Acosta K, Aylward A, Abramson BW, Colt K, Hartwick NT, Liang Y, Shanklin J, Michael TP, Lam E Genomics of turions from the Greater Duckweed reveal pathways for tissue dormancy and reemergence strategy of an aquatic plant.
| 
| 
| Sp9512 can be downloaded from `Michael lab AWS storage <https://salk-tm-pub.s3.us-west-2.amazonaws.com/duckweed/Sp9512.a02_final.tar>`_.
