Anchormap
=========

.. role:: zsh(code)
   :language: zsh

Using the :zsh:`pankmer anchormap` and :zsh:`pankmer view` subcommands, you can anchor a PanKmer index in one or more anchor genomes and launch an interactive dashboard of pangenome statistics. The visualization of :zsh:`pankmer view` is an adaptation of the visualization of `Panagram <https://github.com/kjenike/Panagram>`_ , another k-mer based pangenome tool by Katie Jenike, Sam Kovaka, Shujun Ou, Stephen Hwang, Srividya Ramakrishnan, Ben Langmead, Zach Lippman, and Michael Schatz.

Create an anchormap
-------------------

You can create an anchormap using :zsh:`pankmer anchormap` by providing

1. A PanKmer index
2. A list of anchor genomes
3. Annotation files for each anchor genome in GFF3 format

For example, using the example data directory :zsh:`PanKmer_example_Sp_Chr19/` as shown in the :ref:`tutorial <download_example>`.

.. code-block:: none

   pankmer anchormap -t 2 -i Sp_Chr19_index.tar \
     -o Sp_Chr19_anchormap \
     --anchors Sp_Chr19_genomes/Sp95*.fasta.gz \
     --anno Sp_Chr19_features/*

View an anchormap
-----------------

To view an anchormap:

.. code-block:: none

   pankmer view Sp_Chr19_anchormap/

This will launch a `dash <https://dash.plotly.com/>`_ app, by default serving to localhost at `http://127.0.0.1:8050/ <http://127.0.0.1:8050/>`. You can view the dashboard by opening that address in a web browser.

.. note::

   The :zsh:`pankmer view` subcommand is functional, but is still experimental and under development.

Intermediate example
--------------------

The view illustrated by the tutorial dataset is limited since it includes only one chromosome from each genome. For a more complete example, you can apply :zsh:`pankmer anchormap` and :zsh:`pankmer view` to a few more chromosomes.

Try constructing and viewing an anchormap from an example dataset, for example, the following describes the steps for 4 chromosomes from each of 3 genomes in a subset of the Lemnaceae pangenome.

.. note::

   Executing the following steps should be feasible on a laptop with at least 16 GB of RAM.

Step 1: Download 3 Lemnaceae genomes and their annotatons

.. code-block:: none

   mkdir lemnaceae_genomes lemnaceae_annotations
   pankmer download-example -c Lemnaceae -n 3 -d lemnaceae_genomes
   pankmer download-example -a -c Lemnaceae -n 3 -d lemnaceae_annotations

Step 2: Trim the genome assemblies and annotations to a few small chromosomes

.. code-block:: none

   for g in lemnaceae_genomes/*
     do mv $g temp.gz
     faidx --regex "^chr2(0|1)" temp.gz | bgzip > $g
     rm temp.gz temp.gz.fai
     done
   for a in lemnaceae_annotations/*
     do mv $a temp.gz
     gunzip -c temp.gz | awk '/^##/ || /^chr2(0|1)/' | bgzip > $a
     rm temp.gz
     done

Step 3: Create a PanKmer index

.. code-block:: none

   pankmer index -t 2 -g lemnaceae_genomes -o lemnaceae_index.tar

.. note::

   For larger pangenomes, depending on your RAM resources, you may want to divide indexing into multiple rounds using the :zsh:`--rounds` argument to :zsh:`pankmer index`. Using more rounds will increase runtime but decrease RAM usage.

Step 4: Create and view the anchormap

.. code-block:: none

   pankmer anchormap -t 2 -i lemnaceae_index.tar -o lemnaceae_anchormap \
     --anchors lemnaceae_genomes/*.fasta.gz --anno lemnaceae_annotations/*
   pankmer view lemnaceae_anchormap

.. note::

   For larger pangenomes, depending on your RAM resources, you may want to divide anchormap generation into multiple batches using the :zsh:`--n-batches` argument to :zsh:`pankmer anchormap`. Using more batches will increase runtime but decrease RAM usage.
