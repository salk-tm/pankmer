Indexing algorithm
==================

.. role:: zsh(code)
   :language: zsh

.. figure:: _static/pankmer_diagram_explicit_bitvec.png
   :alt: Diagram of PanKmer functionality

   Diagram of PanKmer functionality.

*k*-mer decomposition
---------------------

PanKmer indexing breaks down genomes to unique canonical *k*-mers (:math:`k=31`), and assigns each *k*-mer a bit vector indicating which samples include it. K-mers are represented as integers as in k-mer counters such as Jellyfish [#jellyfish]_ and Kmer-db [#kmerdb]_. For each *k*-mer in a contig, we encode the *k*-mer and its complement as unsigned 64-bit integers. The larger integer (lexicographically smaller *k*-mer) is selected as the canonical *k*-mer. Each base has a two bit substitute:

.. csv-table:: Nucleotide base to 2-bit integer substitute
   :header: "Base", "2-bit integer"
   
   A,11
   C,10
   G,01
   T,00

For example, the 5-mer ACGTC becomes 11-10-01-00-10 which equals 1938 in base 10. The reverse complement GACGT then becomes 01-11-10-01-00 which equals 1508 in base 10. Since 1938 is greater than 1508, it becomes the representative of the two, and is added to the index. Any *k*-mers containing a base that is not one of A, C, G, or T are skipped.

Each *k*-mer is added/updated in a hash table of *k*-mers wherein the keys are *k*-mers and the values are bit vectors called *scores*. A score indicates presence or absence of the *k*-mer in each sample. Scores are :math:`n` bits where *n* is the number of samples. Each bit indicates presence/absence with sample 1 at the right most bit and sample *n* at the left most bit. For example, for a sample set of four genomes:

.. csv-table:: Example score values
   :header: "Bit Score", "Sample 4", "Sample 3", "Sample 2", "Sample 1"

   0011,"Not present","Not present","Present","Present"
   1101,"Present","Present","Not present","Present"

After all *k*-mers in all contigs have been updated in the hash table, the *k*-mers and scores are written to their corresponding files on disk, along with a metadata file, as shown in the :ref:`tutorial <kmer_index>`.

Choice of *k*
-------------

PanKmer's implementation represents each k-mer as an unsigned 64-bit integer, as in k-mer counting tools such as Jellyfish and Kmer-db [#jellyfish]_ [#kmerdb]_ . Hence the maximum value of *k* is 32. Choosing :math:`k \geq 31` limits the theoretical rate of non-unique k-mers occurring by chance to fewer than 1 per 100 million k-mers [#f3]_. This is important because some of PanKmer's downstream analysis functions, such as genome anchoring, work best when the non-unique k-mer rate is low. Hence, :math:`k=31` and :math:`k=32` are both suitable values, and PanKmer uses :math:`k=31`. Furthermore, 31-bp *k*-mers have been applied successfully to define variation in previous studies [#f2]_ [#f4]_ [#f5]_.


Assembly quality considerations
-------------------------------

The *k*-mer index is robust to varying contiguity of the input assemblies. Chromosome-level assemblies can be compared directly to unscaffolded contigs or even unaligned reads. By the same token, the index is agnostic to the choice of software tools used in the assembly process. However, PanKmer can be confounded by genome sequence datasets that either:

1. Fail to include large sections of the sequenced genome
2. Contain a large number of sequencing errors

In the first case, many *k*-mers will be missing from some genomes in the index. In the second case, many spurious *k*-mers will be injected into some genomes.  So long as the input files do not suffer from extensive sequencing errors or missing reads, the index is agnostic to the choice of assembly pipeline or the contiguity of assemblies.

Numerical complexity, resource usage, & runtime
-----------------------------------------------

Indexing has an :math:`O(n \times c)` memory footprint, where *c* is the number of unique canonical *k*-mers. To reduce the memory usage, the *k*-mer space can be partitioned based on the leading two bases of the canonical *k*-mers. Since we choose the canonical *k*-mer to have a larger value, the *k*-mer space skews to start with AA rather than TT. Thus, the *k*-mer space can be divided into the following groups or any combination thereof: (TT, TG, TC, TA), (GT, GG, GC, GA), (CT, CG), (CC, CA), (AT), (AG), (AC), (AA).

The conversion from contig to *k*-mers to a canonical representative has :math:`O(b)` complexity where *b* is the sum of the genome sizes. The canonical *k*-mers must also be added to the main *k*-mer hash table, which also has :math:`O(b)` complexity. 

.. [#jellyfish] Guillaume Marçais , Carl Kingsford, A fast, lock-free approach for efficient parallel counting of occurrences of k-mers, Bioinformatics, Volume 27, Issue 6, March 2011

.. [#kmerdb] Deorowicz, S., Gudyś, A., Długosz, M., Kokot, M., Danek, A. (2019) Kmer-db: instant evolutionary distance estimation, Bioinformatics

.. [#f2] Rahman, A., Hallgrímsdóttir, I., Eisen, M. & Pachter, L. Association mapping from sequencing reads using k-mers. eLife 7, e32920 (2018).

.. [#f3] Sheikhizadeh, S., Schranz, M. E., Akdel, M., de Ridder, D. & Smit, S. PanTools: representation, storage and exploration of pan-genomic data. Bioinformatics 32, i487–i493 (2016).

.. [#f4] Voichek, Y. & Weigel, D. Identifying genetic variants underlying phenotypic variation in plants without complete genomes. *Nat. Genet.* 52, 534-540 (2020).

.. [#f5] Karikari, B., Lemay, M.-A. & Belzile, F. k-mer-Based Genome-Wide Association Studies in Plants: Advances, Challenges, and Perspectives. *Genes* 14, 1439 (2023).
