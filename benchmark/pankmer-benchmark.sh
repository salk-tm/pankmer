# Typha benchmark
curl -O https://salk-tm-pub.s3.us-west-2.amazonaws.com/pub-supplementary/PRJNA742003-ASSEMBL.tar
tar -xf PRJNA742003-ASSEMBL.tar
rm PRJNA742003-ASSEMBL.tar
pankmer index -g `ls PRJNA742003-ASSEMBL/* | head -n 4` -o typha-index-4genomes.tar \
  --rounds 16 --threads 20 --time > typha-4genomes-16rounds-log.txt
rm typha-index-4genomes.tar
pankmer index -g `ls PRJNA742003-ASSEMBL/* | head -n 4` -o typha-index-4genomes.tar \
  --rounds 256 --threads 20 --time > typha-4genomes-256rounds-log.txt
rm typha-index-4genomes.tar
pankmer index -g PRJNA742003-ASSEMBL/ -o typha-index-13genomes.tar \
  --rounds 16 --threads 20 --time > typha-13genomes-16rounds-log.txt
rm typha-index-13genomes.tar
pankmer index -g PRJNA742003-ASSEMBL/ -o typha-index-13genomes.tar \
  --rounds 256 --threads 20 --time > typha-13genomes-256rounds-log.txt
rm typha-index-13genomes.tar
rm -r PRJNA742003-ASSEMBL

# Solanum benchmark
mkdir Solanum-genomes/
pankmer download-example -d Solanum-genomes/ -c Solanum -n 46
pankmer index -g `ls Solanum-genomes/* | head -n 4` -o solanum-index-4genomes.tar \
  --rounds 16 --threads 20 --time > solanum-4genomes-16rounds-log.txt
rm solanum-index-4genomes.tar
pankmer index -g `ls Solanum-genomes/* | head -n 4` -o solanum-index-4genomes.tar \
  --rounds 256 --threads 20 --time > solanum-4genomes-256rounds-log.txt
rm solanum-index-4genomes.tar
pankmer index -g `ls Solanum-genomes/* | head -n 16` -o solanum-index-16genomes.tar \
  --rounds 16 --threads 20 --time > solanum-16genomes-16rounds-log.txt
rm solanum-index-16genomes.tar
pankmer index -g `ls Solanum-genomes/* | head -n 16` -o solanum-index-16genomes.tar \
  --rounds 256 --threads 20 --time > solanum-16genomes-256rounds-log.txt
rm solanum-index-16genomes.tar
pankmer index -g Solanum-genomes/ -o solanum-index-46genomes.tar \
  --rounds 16 --threads 20 --time > solanum-46genomes-16rounds-log.txt
rm solanum-index-16genomes.tar
pankmer index -g Solanum-genomes/ -o solanum-index-46genomes.tar \
  --rounds 256 --threads 20 --time > solanum-46genomes-256rounds-log.txt
rm solanum-index-16genomes.tar
rm -r Solanum-genomes/

# Zea benchmark
mkdir Zea-genomes/
pankmer download-example -d Zea-genomes/ -c Zea -n 54
pankmer index -g `ls Zea-genomes/* | head -n 4` -o zea-index-4genomes.tar \
  --rounds 16 --threads 20 --time > zea-4genomes-16rounds-log.txt
rm zea-index-4genomes.tar
pankmer index -g `ls Zea-genomes/* | head -n 4` -o zea-index-4genomes.tar \
  --rounds 256 --threads 20 --time > zea-4genomes-256rounds-log.txt
rm zea-index-4genomes.tar
pankmer index -g `ls Zea-genomes/* | head -n 16` -o zea-index-16genomes.tar \
  --rounds 16 --threads 20 --time > zea-16genomes-16rounds-log.txt
rm zea-index-16genomes.tar
pankmer index -g `ls Zea-genomes/* | head -n 16` -o zea-index-16genomes.tar \
  --rounds 256 --threads 20 --time > zea-16genomes-256rounds-log.txt
rm zea-index-16genomes.tar
pankmer index -g Zea-genomes/ -o zea-index-54genomes.tar \
  --rounds 16 --threads 20 --time > zea-54genomes-16rounds-log.txt
rm zea-index-54genomes.tar
pankmer index -g Zea-genomes/ -o zea-index-54.tar \
  --rounds 256 --threads 20 --time > zea-54genomes-256rounds-log.txt
rm zea-index-54genomes.tar
rm -r Zea-genomes/

# Hsapiens benchmark
mkdir Hsapiens-genomes/
pankmer download-example -d Hsapiens-genomes/ -c Hsapiens -n 94
pankmer index -g `ls Hsapiens-genomes/* | head -n 4` -o hsapiens-index-4genomes.tar \
  --rounds 16 --threads 20 --time > hsapiens-4genomes-16rounds-log.txt
rm hsapiens-index-4genomes.tar
pankmer index -g `ls Hsapiens-genomes/* | head -n 4` -o hsapiens-index-4genomes.tar \
  --rounds 256 --threads 20 --time > hsapiens-4genomes-256rounds-log.txt
rm hsapiens-index-4genomes.tar
pankmer index -g `ls Hsapiens-genomes/* | head -n 16` -o hsapiens-index-16genomes.tar \
  --rounds 16 --threads 20 --time > hsapiens-16genomes-16rounds-log.txt
rm hsapiens-index-16genomes.tar
pankmer index -g `ls Hsapiens-genomes/* | head -n 16` -o hsapiens-index-16genomes.tar \
  --rounds 256 --threads 20 --time > hsapiens-16genomes-256rounds-log.txt
rm hsapiens-index-16genomes.tar
pankmer index -g `ls Hsapiens-genomes/* | head -n 64` -o hsapiens-index-64genomes.tar \
  --rounds 16 --threads 20 --time > hsapiens-64genomes-16rounds-log.txt
rm hsapiens-index-64genomes.tar
pankmer index -g `ls Hsapiens-genomes/* | head -n 64` -o hsapiens-index-64genomes.tar \
  --rounds 256 --threads 20 --time > hsapiens-64genomes-256rounds-log.txt
rm hsapiens-index-64genomes.tar
pankmer index -g Hsapiens-genomes/ -o hsapiens-index-94genomes.tar \
  --rounds 16 --threads 20 --time > hsapiens-94genomes-16rounds-log.txt
rm hsapiens-index-94genomes.tar
pankmer index -g Hsapiens-genomes/ -o hsapiens-index-94genomes.tar \
  --rounds 256 --threads 20 --time > hsapiens-94genomes-256rounds-log.txt
rm hsapiens-index-94genomes.tar
rm -r Hsapiens-genomes/

# Athaliana benchmark
mkdir Athaliana-genomes/
pankmer download-example -d Athaliana-genomes/ -c Athaliana -n 1135
pankmer index -g `ls Athaliana-genomes/* | head -n 4` -o athaliana-index-4genomes.tar \
  --rounds 16 --threads 20 --time > athaliana-4genomes-16rounds-log.txt
rm athaliana-index-4genomes.tar
pankmer index -g `ls Athaliana-genomes/* | head -n 4` -o athaliana-index-4genomes.tar \
  --rounds 256 --threads 20 --time > athaliana-4genomes-256rounds-log.txt
rm athaliana-index-4genomes.tar
pankmer index -g `ls Athaliana-genomes/* | head -n 16` -o athaliana-index-16genomes.tar \
  --rounds 16 --threads 20 --time > athaliana-16genomes-16rounds-log.txt
rm athaliana-index-16genomes.tar
pankmer index -g `ls Athaliana-genomes/* | head -n 16` -o athaliana-index-16genomes.tar \
  --rounds 256 --threads 20 --time > athaliana-16genomes-256rounds-log.txt
rm athaliana-index-16genomes.tar
pankmer index -g `ls Athaliana-genomes/* | head -n 64` -o athaliana-index-64genomes.tar \
  --rounds 16 --threads 20 --time > athaliana-64genomes-16rounds-log.txt
rm athaliana-index-64genomes.tar
pankmer index -g `ls Athaliana-genomes/* | head -n 64` -o athaliana-index-64genomes.tar \
  --rounds 256 --threads 20 --time > athaliana-64genomes-256rounds-log.txt
rm athaliana-index-64genomes.tar
pankmer index -g `ls Athaliana-genomes/* | head -n 256` -o athaliana-index-256genomes.tar \
  --rounds 16 --threads 20 --time > athaliana-256genomes-16rounds-log.txt
rm athaliana-index-256genomes.tar
pankmer index -g `ls Athaliana-genomes/* | head -n 256` -o athaliana-index-256genomes.tar \
  --rounds 256 --threads 20 --time > athaliana-256genomes-256rounds-log.txt
rm athaliana-index-256genomes.tar
pankmer index -g Athaliana-genomes/ -o athaliana-index-1135genomes.tar \
  --rounds 16 --threads 20 --time > athaliana-1135genomes-16rounds-log.txt
rm athaliana-index-1135genomes.tar
pankmer index -g Athaliana-genomes/ -o athaliana-index-1135genomes.tar \
  --rounds 256 --threads 20 --time > athaliana-1135genomes-256rounds-log.txt
rm athaliana-index-1135genomes.tar
rm -r Athaliana-genomes/
