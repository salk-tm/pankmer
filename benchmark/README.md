# PanKmer benchmarks

This directory includes two shell scripts, `pankmer-benchmark.sh` and `pankmer-kmerdb-benchmark.sh`. They contain all commands necessary to run the [PanKmer benchmarks](https://salk-tm.gitlab.io/pankmer/large_pangenomes.html#benchmarks) and [PanKmer vs Kmer-db benchmarks](https://salk-tm.gitlab.io/pankmer/large_pangenomes.html#benchmarks) described in the PanKmer documentation.

To run them, you will need an environment with both `pankmer` and `kmer-db` installed. You can set up such an environment with conda by running:

```sh
conda create -c conda-forge -c bioconda -n pankmer-kmerdb cython \
  gff2bed more-itertools pybedtools python-newick pyfaidx \
  rust seaborn upsetplot urllib3 kmer-db
conda activate pankmer-kmerdb
pip install pankmer
```

Then to run the PanKmer benchmarks, you can simply execute:

```sh
sh pankmer-benchmark.sh
```

To run the PanKmer vs Kmer-db benchmarks:

```sh
sh pankmer-benchmark.sh
```

> ### Note
> Running ALL of the benchmarks will take a long time and will require a large amount of available memory. You may wish to select a few lines from each script to run at one time.