# Typha benchmark
curl -O https://salk-tm-pub.s3.us-west-2.amazonaws.com/pub-supplementary/PRJNA742003-ASSEMBL.tar
tar -xf PRJNA742003-ASSEMBL.tar
rm PRJNA742003-ASSEMBL.tar
for file in PRJNA742003-ASSEMBL/*; do gunzip $file; done
ls PRJNA742003-ASSEMBL/* | head -n 4 > typha-samples-4genomes.txt
ls PRJNA742003-ASSEMBL/* > typha-samples-13genomes.txt
kmer-db build -k 30 -t 20 typha-samples-4genomes.txt \
  typha-db-4genomes.db > typha-kmerdb-4genomes-20threads-log.txt
rm typha-db-4genomes.db
kmer-db build -k 30 -t 20 typha-samples-13genomes.txt \
  typha-db-13genomes.db > typha-kmerdb-13genomes-20threads-log.txt
rm typha-db-13genomes.db
kmer-db build -k 30 -t 4 typha-samples-4genomes.txt \
  typha-db-4genomes.db > typha-kmerdb-4genomes-4threads-log.txt
rm typha-db-4genomes.db
kmer-db build -k 30 -t 4 typha-samples-13genomes.txt \
  typha-db-13genomes.db > typha-kmerdb-13genomes-4threads-log.txt
rm typha-db-13genomes.db
pankmer index -g `ls PRJNA742003-ASSEMBL/* | head -n 4` -o typha-index-4genomes.tar \
  --threads 20 --time > typha-pankmer-4genomes-log.txt
rm typha-index-4genomes.tar
pankmer index -g PRJNA742003-ASSEMBL/ -o typha-index-13genomes.tar \
  --threads 20 --time > typha-13genomes-log.txt
rm typha-index-13genomes.tar
rm -r PRJNA742003-ASSEMBL

# Solanum benchmark
mkdir Solanum-genomes/
pankmer download-example -d Solanum-genomes/ -c Solanum -n 16
ls Solanum-genomes/* | head -n 4 > solanum-samples-4genomes.txt
ls Solanum-genomes/* > solanum-samples-16genomes.txt
kmer-db build -k 30 -t 20 solanum-samples-4genomes.txt \
  solanum-db-4genomes.db > solanum-kmerdb-4genomes-20threads-log.txt
rm solanum-db-4genomes.db
kmer-db build -k 30 -t 20 solanum-samples-16genomes.txt \
  solanum-db-16genomes.db > solanum-kmerdb-16genomes-20threads-log.txt
rm solanum-db-16genomes.db
kmer-db build -k 30 -t 4 solanum-samples-4genomes.txt \
  solanum-db-4genomes.db > solanum-kmerdb-4genomes-4threads-log.txt
rm solanum-db-4genomes.db
kmer-db build -k 30 -t 4 solanum-samples-16genomes.txt \
  solanum-db-16genomes.db > solanum-kmerdb-16genomes-4threads-log.txt
rm solanum-db-16genomes.db
pankmer index -g `ls Solanum-genomes/* | head -n 4` -o solanum-index-4genomes.tar \
  --threads 20 --time > solanum-pankmer-4genomes-log.txt
rm solanum-index-4genomes.tar
pankmer index -g Solanum-genomes/ -o solanum-index-16genomes.tar \
  --threads 20 --time > solanum-pankmer-16genomes-log.txt
rm solanum-index-16genomes.tar
rm -r Solanum-genomes/

# Hsapiens benchmark
mkdir Hsapiens-genomes/
pankmer download-example -d Hsapiens-genomes/ -c Hsapiens -n 16
ls Hsapiens-genomes/* | head -n 4 > hsapiens-samples-4genomes.txt
ls Hsapiens-genomes/* > hsapiens-samples-16genomes.txt
kmer-db build -k 30 -t 20 hsapiens-samples-4genomes.txt \
  hsapiens-db-4genomes.db > hsapiens-kmerdb-4genomes-20threads-log.txt
rm hsapiens-db-4genomes.db
kmer-db build -k 30 -t 20 hsapiens-samples-16genomes.txt \
  hsapiens-db-16genomes.db > hsapiens-kmerdb-16genomes-20threads-log.txt
rm hsapiens-db-16genomes.db
kmer-db build -k 30 -t 4 hsapiens-samples-4genomes.txt \
  hsapiens-db-4genomes.db > hsapiens-kmerdb-4genomes-4threads-log.txt
rm hsapiens-db-4genomes.db
kmer-db build -k 30 -t 4 hsapiens-samples-16genomes.txt \
  hsapiens-db-16genomes.db > hsapiens-kmerdb-16genomes-4threads-log.txt
rm hsapiens-db-16genomes.db
pankmer index -g `ls Hsapiens-genomes/* | head -n 4` -o hsapiens-index-4genomes.tar \
  --threads 20 --time > hsapiens-4genomes-log.txt
rm hsapiens-index-4genomes.tar
pankmer index -g `ls Hsapiens-genomes/* | head -n 16` -o hsapiens-index-16genomes.tar \
  --threads 20 --time > hsapiens-16genomes-log.txt
rm hsapiens-index-16genomes.tar
rm -r Hsapiens-genomes/

# Athaliana benchmark
mkdir Athaliana-genomes/
pankmer download-example -d Athaliana-genomes/ -c Athaliana -n 1135
ls Athaliana-genomes/* | head -n 64 > athaliana-samples-64genomes.txt
ls Athaliana-genomes/* | head -n 256 > athaliana-samples-256genomes.txt
ls Athaliana-genomes/* > athaliana-samples-1135genomes.txt
kmer-db build -k 30 -t 20 athaliana-samples-64genomes.txt \
  athaliana-db-64genomes.db > athaliana-kmerdb-64genomes-20threads-log.txt
rm athaliana-db-64genomes.db
kmer-db build -k 30 -t 20 athaliana-samples-256genomes.txt \
  athaliana-db-256genomes.db > athaliana-kmerdb-256genomes-20threads-log.txt
rm athaliana-db-256genomes.db
kmer-db build -k 30 -t 20 athaliana-samples-1135genomes.txt \
  athaliana-db-1135genomes.db > athaliana-kmerdb-1135genomes-20threads-log.txt
rm athaliana-db-1135genomes.db
kmer-db build -k 30 -t 4 athaliana-samples-64genomes.txt \
  athaliana-db-64genomes.db > athaliana-kmerdb-64genomes-4threads-log.txt
rm athaliana-db-64genomes.db
kmer-db build -k 30 -t 4 athaliana-samples-256genomes.txt \
  athaliana-db-256genomes.db > athaliana-kmerdb-256genomes-4threads-log.txt
rm athaliana-db-256genomes.db
kmer-db build -k 30 -t 4 athaliana-samples-1135genomes.txt \
  athaliana-db-1135genomes.db > athaliana-kmerdb-1135genomes-4threads-log.txt
rm athaliana-db-1135genomes.db
pankmer index -g `ls Athaliana-genomes/* | head -n 64` -o athaliana-index-64genomes.tar \
  --threads 20 --time > athaliana-64genomes-log.txt
rm athaliana-index-64genomes.tar
pankmer index -g `ls Athaliana-genomes/* | head -n 256` -o athaliana-index-256genomes.tar \
  --threads 20 --time > athaliana-256genomes-log.txt
rm athaliana-index-256genomes.tar
pankmer index -g Athaliana-genomes/ -o athaliana-index-1135genomes.tar \
  --threads 20 --time > athaliana-1135genomes-log.txt
rm athaliana-index-1135genomes.tar
rm -r Athaliana-genomes/