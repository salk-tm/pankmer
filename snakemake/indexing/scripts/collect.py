from pankmer import PKResults, collect

results = PKResults(snakemake.input.index)
df = collect(
    results,
    snakemake.output.collect_plot,
    width=snakemake.params.width,
    height=snakemake.params.height,
)
df.to_csv(snakemake.output.collect_table)
