import pandas as pd
from pankmer import clustermap

adj_matrix = pd.read_csv(snakemake.input.adjmat, index_col=0)
for output_img in (
    snakemake.output.cluster_pdf,
    snakemake.output.cluster_png,
    snakemake.output.cluster_svg,
):
    clustermap(
        adj_matrix,
        output_img,
        width=snakemake.params.width,
        height=snakemake.params.height,
        metric=snakemake.params.metric,
        method=snakemake.params.method,
    )
