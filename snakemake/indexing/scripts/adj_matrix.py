from pankmer import PKResults, get_adjacency_matrix

df = get_adjacency_matrix(snakemake.input.index)
df.to_csv(snakemake.output.adjmat)
